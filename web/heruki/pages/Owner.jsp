<%@page import="hr.fer.zemris.opp.util.Utility"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>
<html class="html" lang="en-US">
<head>

<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
<title>Vlasnik</title>
<!-- CSS -->
<link rel="stylesheet" type="text/css"
	href="css/site_global.css?4052507572" />
<link rel="stylesheet" type="text/css"
	href="css/index_vlasnik.css?63981491" id="pagesheet" />
</head>
<body>

	<div class="clearfix" id="page">
		<!-- column -->
		<div class="position_content" id="page_position_content">
			<div class="clearfix colelem" id="pu656">
				<!-- group -->
				<div class="clip_frame grpelem" id="u656">
					<!-- image -->
					<img class="block" id="u656_img" src="images/untitled-7.jpg" alt=""
						width="1280" height="400" />
				</div>

				<!-- TEKST NA SLICI -->

				<div class="clearfix grpelem" id="u87-6">
					<!-- content -->
					<p id="u87-2">APARTMANI HERUKI</p>
					<p id="u87-4">
						<c:choose>
							<c:when test="${hasInfo}">${info}</c:when>
							<c:otherwise>Vlasnik</c:otherwise>
						</c:choose>
					</p>
				</div>

				<jsp:include page="Header.jsp" />

			</div>
			<div class="clearfix colelem" id="pu570-4">
				<!-- group -->
				<div class="clearfix grpelem" id="u570-4">
					<!-- content -->
					<p>Izaberite izvještaj</p>
				</div>

				<!--  FORM ZA ADMINA -->

				<form class="form-grp clearfix grpelem" id="widgetu916" method="get"
					action="<%=request.getContextPath()%>/vlasnik">
					<div class="fld-grp clearfix grpelem" id="widgetu919"
						data-required="true" data-type="email">
						<!-- none box -->
						<label class="fld-label actAsDiv clearfix grpelem" id="u922-4"
							for="widgetu919_input"> <!-- content --> <span
							class="actAsPara">Postavi admina:</span>
						</label> <span class="fld-input NoWrap actAsDiv clearfix grpelem"
							id="u921-3"> <!-- content --> <input class="wrapped-input"
							type="text" spellcheck="false" id="widgetu919_input"
							name="adminNick" value="admin" tabindex="1" />
						</span>
					</div>
					<input class="submit-btn NoWrap grpelem" id="u928-17" type="submit"
						value="" tabindex="2" />
					<!-- state-based BG images -->
				</form>
			</div>

			<!-- FORM ZA IZVJESTAJ -->

			<form class="form-grp clearfix colelem" id="widgetu844" method="post"
				action="<%=request.getContextPath()%>/vlasnik">
				<!-- none box -->

				<div class="fld-grp clearfix grpelem" id="widgetu857"
					data-required="true">
					<!-- none box -->
					<label class="fld-label actAsDiv clearfix grpelem" id="u859-4"
						for="widgetu857_input"> <!-- content --> <span
						class="actAsPara">Datum od</span>
					</label> <span class="fld-input NoWrap actAsDiv clearfix grpelem"
						id="u860-4"> <!-- content --> <input checked
						class="wrapped-input" type="date" name="from"
						value='<%=Utility.todaysDate()%>' id="widgetu857_input"
						tabindex="4" />
					</span>
				</div>
				<div class="fld-grp clearfix grpelem" id="widgetu847"
					data-required="true" data-type="email">
					<!-- none box -->
					<label class="fld-label actAsDiv clearfix grpelem" id="u850-4"
						for="widgetu847_input"> <!-- content --> <span
						class="actAsPara">Datum do</span>
					</label> <span class="fld-input NoWrap actAsDiv clearfix grpelem"
						id="u849-4"> <!-- content --> <input class="wrapped-input"
						type="date" name="to" value='<%=Utility.todaysDate()%>'
						id="widgetu847_input" tabindex="5" />
					</span>
				</div>

				<input class="submit-btn NoWrap grpelem" id="u856-17" type="submit"
					value="" tabindex="6" />
				<!-- state-based BG images -->
				<div class="fld-grp clearfix grpelem" id="widgetu861"
					data-required="true" data-type="radiogroup">
					<!-- none box -->
					<div class="fld-grp clearfix grpelem" id="widgetu865"
						data-required="false" data-type="radio">
						<!-- none box -->
						<label class="fld-label actAsDiv clearfix grpelem" id="u866-4"
							for="widgetu865_input"> <!-- content --> <span
							class="actAsPara">Smještajne jedinice</span>
						</label>
						<div class="fld-radiobutton museBGSize grpelem" id="u868">
							<!-- simple frame -->
							<input class="wrapped-input" type="radio" value="1"
								spellcheck="false" id="widgetu865_input" name="id" tabindex="3" />
							<label for="widgetu865_input"></label>
						</div>
					</div>
					<div class="fld-grp clearfix grpelem" id="widgetu899"
						data-required="false" data-type="radio">
						<!-- none box -->
						<label class="fld-label actAsDiv clearfix grpelem" id="u901-4"
							for="widgetu899_input"> <!-- content --> <span
							class="actAsPara">Najtraženije usluge</span>
						</label>
						<div class="fld-radiobutton museBGSize grpelem" id="u900">
							<!-- simple frame -->
							<input class="wrapped-input" type="radio" value="2"
								spellcheck="false" id="widgetu899_input" name="id" tabindex="3" />
							<label for="widgetu899_input"></label>
						</div>
					</div>
					<div class="fld-grp clearfix grpelem" id="widgetu903"
						data-required="false" data-type="radio">
						<!-- none box -->
						<label class="fld-label actAsDiv clearfix grpelem" id="u904-4"
							for="widgetu903_input"> <!-- content --> <span
							class="actAsPara">Pregled po gradovima</span>
						</label>
						<div class="fld-radiobutton museBGSize grpelem" id="u905">
							<!-- simple frame -->
							<input class="wrapped-input" type="radio" value="3"
								spellcheck="false" id="widgetu903_input" name="id" tabindex="3" />
							<label for="widgetu903_input"></label>
						</div>
					</div>
					<div class="fld-grp clearfix grpelem" id="widgetu906"
						data-required="false" data-type="radio">
						<!-- none box -->
						<label class="fld-label actAsDiv clearfix grpelem" id="u907-4"
							for="widgetu906_input"> <!-- content --> <span
							class="actAsPara">Pregled po državama</span>
						</label>
						<div class="fld-radiobutton museBGSize grpelem" id="u908">
							<!-- simple frame -->
							<input class="wrapped-input" type="radio" value="4"
								spellcheck="false" id="widgetu906_input" name="id" tabindex="3" />
							<label for="widgetu906_input"></label>
						</div>
					</div>
				</div>
			</form>

			<c:if test="${hasParams}">
				<div class="clearfix colelem" id="pu909-4">
					<!-- group -->
					<div class="clearfix grpelem" id="u909-4">
						<!-- content -->
						<p>
							<a id="blackcolor"
								href="<%=request.getContextPath()%>/report?t=2&${params}">Skini
								u XLS formatu</a>
						</p>
					</div>
					<div class="clip_frame grpelem" id="u910">
						<!-- image -->
						<img class="block" id="u910_img"
							src="<%=request.getContextPath()%>/report?t=1&${params}" alt=""
							width="458" height="296" />
					</div>
				</div>
				<div class="verticalspacer"></div>
			</c:if>
		</div>
	</div>

	<div class="preload_images">
		<img class="preload" src="images/u928-17-r.png" alt="" /> <img
			class="preload" src="images/u928-17-m.png" alt="" /> <img
			class="preload" src="images/u928-17-fs.png" alt="" /> <img
			class="preload" src="images/u856-17-r.png" alt="" /> <img
			class="preload" src="images/u856-17-m.png" alt="" /> <img
			class="preload" src="images/u856-17-fs.png" alt="" /> <img
			class="preload" src="images/radiobuttonunchecked.png" alt="" /> <img
			class="preload" src="images/radiobuttonuncheckedrollover.png" alt="" />
		<img class="preload" src="images/radiobuttonuncheckedmousedown.png"
			alt="" /> <img class="preload" src="images/radiobuttonchecked.png"
			alt="" /> <img class="preload"
			src="images/radiobuttoncheckedrollover.png" alt="" /> <img
			class="preload" src="images/radiobuttoncheckedmousedown.png" alt="" />
	</div>

</body>
</html>
