<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html class="html" lang="en-US">
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
<title>Registracija</title>
<!-- CSS -->
<link rel="stylesheet" type="text/css"
	href="css/site_global.css?4052507572" />
<link rel="stylesheet" type="text/css"
	href="css/index_registracija.css?79002234" id="pagesheet" />
</head>
<body>

	<div class="clearfix" id="page">
		<!-- column -->
		<div class="position_content" id="page_position_content">
			<div class="clearfix colelem" id="pu692">
				<!-- group -->
				<div class="clip_frame grpelem" id="u692">
					<!-- image -->
					<img class="block" id="u692_img" src="images/untitled-6.jpg" alt=""
						width="1280" height="600" />
				</div>
				<div class="browser_width grpelem" id="u75-bw">
					<div id="u75">
						<!-- simple frame -->
					</div>
				</div>

				<!-- NA SLICI -->

				<div class="clearfix grpelem" id="u87-6">
					<!-- content -->
					<p id="u87-2">APARTMANI HERUKI</p>
					<p id="u87-4">
						<c:choose>
							<c:when test="${form.hasErrorFor('nickname')}">
								<c:out value="${form.getErrorFor('nickname')}" />
							</c:when>
							<c:when test="${form.hasErrorFor('password')}">
								<c:out value="${form.getErrorFor('password')}" />
							</c:when>
							<c:when test="${form.hasErrorFor('email')}">
								<c:out value="${form.getErrorFor('email')}" />
							</c:when>
							<c:otherwise>Prijavite se!</c:otherwise>
						</c:choose>
					</p>
				</div>

				<jsp:include page="Header.jsp" />

				<!-- FORM -->

				<form class="form-grp clearfix grpelem" id="widgetu698"
					action="<%=request.getContextPath()%>/registracija" method="post">
					<!-- none box -->

					<div class="fld-grp clearfix grpelem" id="widgetu699"
						data-required="true">
						<!-- none box -->
						<label class="fld-label actAsDiv clearfix grpelem" id="u702-4"
							for="widgetu699_input"> <!-- content --> <span
							class="actAsPara">Nadimak:</span>
						</label> <span class="fld-input NoWrap actAsDiv clearfix grpelem"
							id="u700-4"> <!-- content --> <input class="wrapped-input"
							type="text" spellcheck="false" id="widgetu699_input"
							name="nickname" value='<c:out value="${form.nickname}"/>'
							required tabindex="1" />
						</span>
					</div>

					<div class="fld-grp clearfix grpelem" id="widgetu711"
						data-required="true" data-type="email">
						<!-- none box -->
						<label class="fld-label actAsDiv clearfix grpelem" id="u713-4"
							for="widgetu711_input"> <!-- content --> <span
							class="actAsPara">Lozinka:</span>
						</label> <span class="fld-input NoWrap actAsDiv clearfix grpelem"
							id="u714-4"> <!-- content --> <input class="wrapped-input"
							type="password" spellcheck="false" id="widgetu711_input"
							name="password" value="" size="25" required tabindex="2" />
						</span>
					</div>

					<input class="submit-btn NoWrap grpelem" id="u710-17" type="submit"
						value="" tabindex="11" autofocus />
					<!-- state-based BG images -->

					<div class="fld-grp clearfix grpelem" id="widgetu715"
						data-required="true">
						<!-- none box -->
						<label class="fld-label actAsDiv clearfix grpelem" id="u716-4"
							for="widgetu715_input"> <!-- content --> <span
							class="actAsPara">Ponovi lozinku:</span>
						</label> <span class="fld-input NoWrap actAsDiv clearfix grpelem"
							id="u718-4"> <!-- content --> <input class="wrapped-input"
							type="password" spellcheck="false" id="widgetu715_input"
							name="password2" value="" required tabindex="3" />
						</span>
					</div>

					<div class="fld-grp clearfix grpelem" id="widgetu719"
						data-required="true">
						<!-- none box -->
						<label class="fld-label actAsDiv clearfix grpelem" id="u722-4"
							for="widgetu719_input"> <!-- content --> <span
							class="actAsPara">Ime:</span>
						</label> <span class="fld-input NoWrap actAsDiv clearfix grpelem"
							id="u720-4"> <!-- content --> <input class="wrapped-input"
							type="text" spellcheck="false" id="widgetu719_input"
							name="firstname" value='<c:out value="${form.firstname}"/>'
							required tabindex="4" />
						</span>
					</div>

					<div class="fld-grp clearfix grpelem" id="widgetu723"
						data-required="true">
						<!-- none box -->
						<label class="fld-label actAsDiv clearfix grpelem" id="u726-4"
							for="widgetu723_input"> <!-- content --> <span
							class="actAsPara">Prezime:</span>
						</label> <span class="fld-input NoWrap actAsDiv clearfix grpelem"
							id="u724-4"> <!-- content --> <input class="wrapped-input"
							type="text" spellcheck="false" id="widgetu723_input"
							name="lastname" value='<c:out value="${form.lastname}"/>'
							required tabindex="5" />
						</span>
					</div>

					<div class="fld-grp clearfix grpelem" id="widgetu727"
						data-required="true">
						<!-- none box -->
						<label class="fld-label actAsDiv clearfix grpelem" id="u729-4"
							for="widgetu727_input"> <!-- content --> <span
							class="actAsPara">E-mail:</span>
						</label> <span class="fld-input NoWrap actAsDiv clearfix grpelem"
							id="u730-4"> <!-- content --> <input class="wrapped-input"
							type="email" spellcheck="false" id="widgetu727_input"
							name="email" value='<c:out value="${form.email}"/>' required
							tabindex="6" />
						</span>
					</div>

					<div class="fld-grp clearfix grpelem" id="widgetu751"
						data-required="true">
						<!-- none box -->
						<label class="fld-label actAsDiv clearfix grpelem" id="u753-4"
							for="widgetu751_input"> <!-- content --> <span
							class="actAsPara">Telefon:</span>
						</label> <span class="fld-input NoWrap actAsDiv clearfix grpelem"
							id="u754-4"> <!-- content --> <input class="wrapped-input"
							type="text" spellcheck="false" id="widgetu751_input"
							name="telephone" value='<c:out value="${form.telephone}"/>'
							tabindex="7" />
						</span>
					</div>

					<!-- ADRESA -->

					<div class="fld-grp clearfix grpelem" id="widgetu755"
						data-required="true">
						<!-- none box -->
						<label class="fld-label actAsDiv clearfix grpelem" id="u756-4"
							for="widgetu755_input"> <!-- content --> <span
							class="actAsPara">Adresa:</span>
						</label> <span class="fld-input NoWrap actAsDiv clearfix grpelem"
							id="u758-4"> <!-- content --> <input class="wrapped-input"
							type="text" spellcheck="false" id="widgetu755_input"
							name="street" value='<c:out value="${form.street}"/>' required
							tabindex="8" />
						</span>
					</div>

					<div class="fld-grp clearfix grpelem" id="widgetu759"
						data-required="true">
						<!-- none box -->
						<label class="fld-label actAsDiv clearfix grpelem" id="u761-4"
							for="widgetu759_input"> <!-- content --> <span
							class="actAsPara">Grad:</span>
						</label> <span class="fld-input NoWrap actAsDiv clearfix grpelem"
							id="u762-4"> <!-- content --> <input class="wrapped-input"
							type="text" spellcheck="false" id="widgetu759_input" name="city"
							value='<c:out value="${form.city}"/>' required tabindex="9" />
						</span>
					</div>

					<div class="fld-grp clearfix grpelem" id="widgetu763"
						data-required="true">
						<!-- none box -->
						<label class="fld-label actAsDiv clearfix grpelem" id="u765-4"
							for="widgetu763_input"> <!-- content --> <span
							class="actAsPara">Država:</span>
						</label> <span class="fld-input NoWrap actAsDiv clearfix grpelem"
							id="u766-4"> <!-- content --> <select
							class="wrapped-input" id="widgetu763_input" name="country"
							required tabindex="10">
								<c:forEach var="c" items="${countries}">
									<option value="${c.id}">${c.name}</option>
								</c:forEach>
						</select>
						</span>
					</div>
				</form>

			</div>
		</div>
	</div>
</body>
</html>
