<%@page import="hr.fer.zemris.opp.util.Utility"%>
<%@page import="web.servleti.UserCounter"%>
<%@page import="hr.fer.zemris.opp.model.User"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html class="html" lang="en-US">
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
<title>Ponuda</title>
<!-- CSS -->
<link rel="stylesheet" type="text/css"
	href="css/site_global.css?4052507572" />
<link rel="stylesheet" type="text/css"
	href="css/index_ponuda.css?4223799257" id="pagesheet" />
</head>
<body>

	<div class="clearfix" id="page">
		<!-- column -->
		<div class="position_content" id="page_position_content">
			<div class="clearfix colelem" id="pu656">
				<!-- group -->
				<div class="clip_frame grpelem" id="u656">
					<!-- image -->
					<img class="block" id="u656_img" src="images/untitled-7.jpg" alt=""
						width="1280" height="400" />
				</div>

				<!-- SLIKA -->

				<div class="clearfix grpelem" id="u87-6">
					<!-- content -->
					<p id="u87-2">APARTMANI HERUKI</p>
					<p id="u87-4">Rezervirajte!</p>
				</div>

				<jsp:include page="Header.jsp" />

			</div>

			<div class="clearfix colelem" id="u570-4">
				<!-- content -->
				<p>Rezervirajte sobu</p>
			</div>
			<div class="clearfix colelem" id="pu576">
				<!-- group -->
				<div class="clip_frame grpelem" id="u576">
					<!-- image -->
					<img class="block" id="u576_img" src="images/sobe/${g.picture}.png"
						alt="" width="214" height="160" />
				</div>
				<div class="clearfix grpelem" id="pu663-4">
					<!-- column -->
					<div class="clearfix colelem" id="u663-4">
						<!-- content -->
						<p>${g.name}</p>
					</div>
					<div class="clearfix colelem" id="u606-11">
						<!-- content -->
						<p>${g.description}</p>
						<p>Smjestaj je slobodan od ${dateFrom} do ${dateTo}</p>
						<p>Cijena iznosi ${price} kn</p>
						<c:if test="${!loggedIn}">
							<p>
								<a id="blackcolor" href="<%=request.getContextPath()%>/prijava">Prijavite
									se</a> ili <a id="blackcolor"
									href="<%=request.getContextPath()%>/registracija">registrirajte
								</a>
							</p>
						</c:if>
					</div>

				</div>
				<form class="form-grp clearfix grpelem" id="widgetu664"
					method="post" action="<%=request.getContextPath()%>/rezervacija">
					<c:if test='${loggedIn}'>
						<input class="submit-btn NoWrap grpelem" id="u676-17"
							type="submit" value="" tabindex="4" />
					</c:if>
					<input type="hidden" name="gid" value='${g.id}' />
					<!-- state-based BG images -->
					<div class="fld-grp clearfix grpelem" id="widgetu740"
						data-required="true" data-type="checkbox">
						<!-- none box -->
						<label class="fld-label actAsDiv clearfix grpelem" id="u743-5"
							for="widgetu740_input"> <!-- content --> <span
							class="actAsPara">${services[0].name}</span>
						</label>
						<div class="fld-checkbox museBGSize grpelem" id="u744">
							<!-- simple frame -->
							<input class="wrapped-input" type="checkbox"
								value="${services[0].id}" id="widgetu740_input" name="s"
								tabindex="1" /> <label for="widgetu740_input"></label>
						</div>
					</div>
					<div class="fld-grp clearfix grpelem" id="widgetu776"
						data-required="true" data-type="checkbox">
						<!-- none box -->
						<label class="fld-label actAsDiv clearfix grpelem" id="u778-5"
							for="widgetu776_input"> <!-- content --> <span
							class="actAsPara">${services[1].name}</span>
						</label>
						<div class="fld-checkbox museBGSize grpelem" id="u777">
							<!-- simple frame -->
							<input class="wrapped-input" type="checkbox"
								value="${services[1].id}" id="widgetu776_input" name="s"
								tabindex="2" /> <label for="widgetu776_input"></label>
						</div>
					</div>
					<div class="fld-grp clearfix grpelem" id="widgetu780"
						data-required="true" data-type="checkbox">
						<!-- none box -->
						<label class="fld-label actAsDiv clearfix grpelem" id="u782-5"
							for="widgetu780_input"> <!-- content --> <span
							class="actAsPara">${services[2].name}</span>
						</label>
						<div class="fld-checkbox museBGSize grpelem" id="u781">
							<!-- simple frame -->
							<input class="wrapped-input" type="checkbox"
								value="${services[2].id}" id="widgetu780_input" name="s"
								tabindex="3" /> <label for="widgetu780_input"></label>
						</div>
					</div>
				</form>
			</div>
			<div class="verticalspacer"></div>
		</div>
	</div>
	<div class="preload_images">
		<img class="preload" src="images/u676-17-r.png" alt="" /> <img
			class="preload" src="images/u676-17-m.png" alt="" /> <img
			class="preload" src="images/u676-17-fs.png" alt="" /> <img
			class="preload" src="images/radiobuttonunchecked.png" alt="" /> <img
			class="preload" src="images/radiobuttonuncheckedrollover.png" alt="" />
		<img class="preload" src="images/radiobuttonuncheckedmousedown.png"
			alt="" /> <img class="preload" src="images/radiobuttonchecked.png"
			alt="" /> <img class="preload"
			src="images/radiobuttoncheckedrollover.png" alt="" /> <img
			class="preload" src="images/radiobuttoncheckedmousedown.png" alt="" />
	</div>
</body>
</html>
