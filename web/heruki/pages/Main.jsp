<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="hr.fer.zemris.opp.util.Utility"%>


<!DOCTYPE html>
<html class="html" lang="en-US">
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
<title>Pocetna</title>
<!-- CSS -->
<link rel="stylesheet" type="text/css"
	href="css/site_global.css?453856629" />
<link rel="stylesheet" type="text/css" href="css/index.css?3821046040"
	id="pagesheet" />
</head>
<body>

	<div class="clearfix" id="page">
		<!-- column -->
		<div class="position_content" id="page_position_content">
			<div class="clearfix colelem" id="pu692">
				<!-- group -->
				<div class="clip_frame grpelem" id="u692">
					<!-- image -->
					<img class="block" id="u692_img" src="images/untitled-6.jpg" alt=""
						width="1280" height="600" />
				</div>

				<div class="browser_width grpelem" id="u75-bw">
					<div id="u75">
						<!-- simple frame -->
					</div>
				</div>

				<!-- NASLOV GLAVNI -->

				<div class="clearfix grpelem" id="u87-6">
					<!-- content -->
					<p id="u87-2">APARTMANI HERUKI</p>
					<p id="u87-4">
						<c:choose>
							<c:when test="${form.hasErrorFor('date')}">
								<c:out value="${form.getErrorFor('date')}" />
							</c:when>
							<c:otherwise>Rezervirajte sobu! ${error}</c:otherwise>
						</c:choose>
					</p>
				</div>

				<jsp:include page="Header.jsp" />

				<!-- SEARCH FORM -->

				<div class="clearfix grpelem" id="u689-4">
					<!-- content -->
					<p>Osobe:</p>
				</div>
				<div class="clearfix grpelem" id="u690-4">
					<!-- content -->
					<p>Djeca:</p>
				</div>
				<div class="clearfix grpelem" id="u691-4">
					<!-- content -->
					<p>Dob:</p>
				</div>
				<div class="clearfix grpelem" id="u564-4">
					<!-- content -->
					<p>Vrijeme boravka:</p>
				</div>

				<form class="form-grp clearfix grpelem" id="widgetu400"
					method="post" action="<%=request.getContextPath()%>/pretraga">
					<!-- none box -->
					<div class="fld-grp clearfix grpelem" id="widgetu413"
						data-required="true">
						<!-- none box -->
						<span class="fld-input NoWrap actAsDiv clearfix grpelem"
							id="u416-4"> <!-- content --> <input class="wrapped-input"
							type="number" name="adults" min="1" max="1000" required
							spellcheck="false" id="widgetu413_input" tabindex="1" />
						</span>
					</div>
					<div class="fld-grp clearfix grpelem" id="widgetu403"
						data-required="true" data-type="email">
						<!-- none box -->
						<span class="fld-input NoWrap actAsDiv clearfix grpelem"
							id="u405-4"> <!-- content --> <input class="wrapped-input"
							type="number" name="children" min="0" max="1000" value="0"
							required spellcheck="false" id="widgetu403_input" tabindex="2" />
						</span>
					</div>
					<input class="submit-btn NoWrap grpelem" id="u412-17" type="submit"
						value="" tabindex="7" />
					<!-- state-based BG images -->
					<div class="fld-grp clearfix grpelem" id="widgetu417"
						data-required="true">
						<!-- none box -->
						<span class="fld-input NoWrap actAsDiv clearfix grpelem"
							id="u419-4"> <!-- content --> <select
							class="wrapped-input" tabindex="3" name="childrenAge"
							id="widgetu417_input">
								<option value="0-2">0-2</option>
								<option value="2-7">2-7</option>
								<option value="8-14">8-14</option>
						</select>
						</span>
					</div>

					<div class="fld-grp clearfix grpelem" id="widgetu421"
						data-required="true">
						<!-- none box -->
						<span class="fld-input NoWrap actAsDiv clearfix grpelem"
							id="u424-4"> <!-- content --> <input class="wrapped-input"
							id="widgetu421_input" type="date" name="dateFrom"
							value='<%=Utility.todaysDate()%>' required tabindex="4" />
						</span>
					</div>
					<div class="fld-grp clearfix grpelem" id="widgetu425"
						data-required="true">
						<!-- none box -->
						<span class="fld-input NoWrap actAsDiv clearfix grpelem"
							id="u426-4"> <!-- content --> <input class="wrapped-input"
							id="widgetu425_input" type="date" name="dateTo"
							value='<%=Utility.todaysDate()%>' required tabindex="5" />
						</span>
					</div>
					<div class="fld-grp clearfix grpelem" id="widgetu429"
						data-required="true" data-type="radiogroup">
						<!-- none box -->
						<label class="fld-label actAsDiv clearfix grpelem" id="u432-4">
							<!-- content --> <span class="actAsPara">Pogled na:</span>
						</label>
						<div class="fld-grp clearfix grpelem" id="widgetu433"
							data-required="false" data-type="radio">
							<!-- none box -->
							<label class="fld-label actAsDiv clearfix grpelem" id="u434-4"
								for="widgetu433_input"> <!-- content --> <span
								class="actAsPara">More</span>
							</label>
							<div class="fld-radiobutton museBGSize grpelem" id="u436">
								<!-- simple frame -->
								<input class="wrapped-input" type="radio" spellcheck="false"
									id="widgetu433_input" tabindex="6" name="view" value="more"
									checked /> <label for="widgetu433_input"></label>
							</div>
						</div>
						<div class="fld-grp clearfix grpelem" id="widgetu467"
							data-required="false" data-type="radio">
							<!-- none box -->
							<label class="fld-label actAsDiv clearfix grpelem" id="u468-4"
								for="widgetu467_input"> <!-- content --> <span
								class="actAsPara">Park</span>
							</label>
							<div class="fld-radiobutton museBGSize grpelem" id="u469">
								<!-- simple frame -->
								<input class="wrapped-input" type="radio" name="view"
									value="park" spellcheck="false" id="widgetu467_input"
									tabindex="6" /> <label for="widgetu467_input"></label>
							</div>
						</div>
					</div>
				</form>

			</div>
			<div class="clearfix colelem" id="u570-4">
				<!-- content -->
				<p>Dobrodošli!</p>
			</div>
			<div class="clearfix colelem" id="u571-4">
				<!-- content -->
				<p>
					<br>Apartmani Heruki smješteni u Sunčanoj uvali, udaljeni su
					svega 50 metara od mora i sportskih sadržaja te 20 minuta lagane
					šetnje od centra Malog Lošinja.
				</p>
				<p>Putujete li sami ili u paru, apartmani Heruki idealan su
					izbor za romantičan i opuštajući odmor uz more. Uronjen u zelenilo
					stoljetne borove šume, spoj je modernog dizajna, udobnosti i
					vrhunske usluge.</p>
				<p>
					<br>
				</p>
			</div>
			<div class="clearfix colelem" id="ppu582">
				<!-- group -->
				<div class="clearfix grpelem" id="pu582">
					<!-- group -->
					<div class="clip_frame grpelem" id="u582">
						<!-- image -->
						<img class="block" id="u582_img" src="images/vile/2.jpg" alt=""
							width="214" height="160" />
					</div>
					<div class="clearfix grpelem" id="u607-4">
						<!-- content -->
						<p>Villa Medić</p>
					</div>
				</div>
				<div class="clearfix grpelem" id="pu614">
					<!-- group -->
					<div class="clip_frame grpelem" id="u614">
						<!-- image -->
						<img class="block" id="u614_img" src="images/vile/4.jpg" alt=""
							width="214" height="160" />
					</div>
					<div class="clearfix grpelem" id="u609-4">
						<!-- content -->
						<p>Villa Roko</p>
					</div>
				</div>
				<div class="clip_frame grpelem" id="u576">
					<!-- image -->
					<img class="block" id="u576_img" src="images/vile/1.jpg" alt=""
						width="214" height="160" />
				</div>
				<div class="clip_frame grpelem" id="u587">
					<!-- image -->
					<img class="block" id="u587_img" src="images/vile/3.jpg" alt=""
						width="214" height="160" />
				</div>
				<div class="clearfix grpelem" id="u606-4">
					<!-- content -->
					<p>Villa Hrenić</p>
				</div>
				<div class="clearfix grpelem" id="u608-4">
					<!-- content -->
					<p>Villa Masters</p>
				</div>
			</div>
			<div class="verticalspacer"></div>
		</div>
	</div>
	<div class="preload_images">
		<img class="preload" src="images/u412-17-r.png" alt="" /> <img
			class="preload" src="images/u412-17-m.png" alt="" /> <img
			class="preload" src="images/u412-17-fs.png" alt="" /> <img
			class="preload" src="images/radiobuttonunchecked.png" alt="" /> <img
			class="preload" src="images/radiobuttonuncheckedrollover.png" alt="" />
		<img class="preload" src="images/radiobuttonuncheckedmousedown.png"
			alt="" /> <img class="preload" src="images/radiobuttonchecked.png"
			alt="" /> <img class="preload"
			src="images/radiobuttoncheckedrollover.png" alt="" /> <img
			class="preload" src="images/radiobuttoncheckedmousedown.png" alt="" />
	</div>
</body>
</html>
