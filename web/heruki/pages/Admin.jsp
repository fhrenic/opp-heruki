<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html class="html" lang="en-US">
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
<title>Administrator</title>
<!-- CSS -->
<link rel="stylesheet" type="text/css"
	href="css/site_global.css?4052507572" />
<link rel="stylesheet" type="text/css" href="css/index_admin.css?112207570"
	id="pagesheet" />
</head>
<body>

	<div class="clearfix" id="page">
		<!-- column -->
		<div class="position_content" id="page_position_content">
			<div class="clearfix colelem" id="pu692">
				<!-- group -->
				<div class="clip_frame grpelem" id="u692">
					<!-- image -->
					<img class="block" id="u692_img" src="images/untitled-6.jpg" alt=""
						width="1280" height="600" />
				</div>
				<div class="browser_width grpelem" id="u75-bw">
					<div id="u75">
						<!-- simple frame -->
					</div>
				</div>

				<jsp:include page="Header.jsp" />

				<div class="clearfix grpelem" id="u87-6">
					<!-- content -->
					<p id="u87-2">APARTMANI HERUKI</p>
					<p id="u87-4">
						<c:choose>
							<c:when test="${hasInfo}">${info}</c:when>
							<c:otherwise>Potvrdi ili izbrisi registraciju</c:otherwise>
						</c:choose>
					</p>
				</div>

				<form class="form-grp clearfix grpelem" id="widgetu698"
					method="post" action="<%=request.getContextPath()%>/admin">
					<!-- none box -->
					<div class="fld-grp clearfix grpelem" id="widgetu699"
						data-required="true">
						<!-- none box -->
						<label class="fld-label actAsDiv clearfix grpelem" id="u702-4"
							for="widgetu699_input"> <!-- content --> <span
							class="actAsPara">Potvrdi rezervaciju:</span>
						</label> <span class="fld-input NoWrap actAsDiv clearfix grpelem"
							id="u700-4"> <!-- content --> <input class="wrapped-input"
							type="number" id="widgetu699_input" name="potvrdi" min="1"
							tabindex="1" />
						</span>
					</div>
					<div class="fld-grp clearfix grpelem" id="widgetu711"
						data-required="true" data-type="email">
						<!-- none box -->
						<label class="fld-label actAsDiv clearfix grpelem" id="u713-4"
							for="widgetu711_input"> <!-- content --> <span
							class="actAsPara">Izbriši rezervaciju:</span>
						</label> <span class="fld-input NoWrap actAsDiv clearfix grpelem"
							id="u714-4"> <!-- content --> <input class="wrapped-input"
							type="number" id="widgetu711_input" name="izbrisi" min="1"
							tabindex="2" />
						</span>
					</div>

					<input class="submit-btn NoWrap grpelem" id="u710-17" type="submit"
						value="" tabindex="3" />
					<!-- state-based BG images -->
				</form>
			</div>
			<div class="verticalspacer"></div>
		</div>
	</div>
	<div class="preload_images">
		<img class="preload" src="images/u710-17-r.png" alt="" /> <img
			class="preload" src="images/u710-17-m.png" alt="" /> <img
			class="preload" src="images/u710-17-fs.png" alt="" />
	</div>

</body>
</html>
