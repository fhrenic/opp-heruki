<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>
<html class="html" lang="en-US">
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
<title>Info</title>
<!-- CSS -->
<link rel="stylesheet" type="text/css"
	href="css/site_global.css?4052507572" />
<link rel="stylesheet" type="text/css"
	href="css/index_info.css?3925342673" id="pagesheet" />
</head>
<body>

	<div class="clearfix" id="page">
		<!-- column -->
		<div class="position_content" id="page_position_content">
			<div class="clearfix colelem" id="pu656">
				<!-- group -->
				<div class="clip_frame grpelem" id="u656">
					<!-- image -->
					<img class="block" id="u656_img" src="images/untitled-7.jpg" alt=""
						width="1280" height="400" />
				</div>

				<div class="clearfix grpelem" id="u87-4">
					<!-- content -->
					<p>APARTMANI HERUKI</p>
				</div>

				<jsp:include page="Header.jsp" />

				<div class="clearfix colelem" id="u570-4">
					<!-- content -->
					<p>${infoHeading}</p>
				</div>
				<div class="clearfix colelem" id="u606-8">
					<!-- content -->
					<p>${infoParagraph}</p>
				</div>
				<div class="verticalspacer"></div>
			</div>
		</div>
	</div>
</body>
</html>
