<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="web.servleti.UserCounter"%>
<%@page import="hr.fer.zemris.opp.model.User"%>


<!-- POCETNA -->
<div class="clearfix grpelem" id="u610-4">
	<!-- content -->
	<p>
		<a href="<%=request.getContextPath()%>/">APARTMANI HERUKI</a>
	</p>
</div>

<!-- KORISNIK -->

<%
    if (session.getAttribute("current.user") != null) {
        User user = (User) session.getAttribute("current.user");
%>


<div class="clearfix grpelem" id="u76-5">
	<!-- content -->

	<p>
		<span id="u76-2"> <c:choose>
				<c:when test="<%=user.getAuthLevel() == User.OWNER%>">
					<a href="<%=request.getContextPath()%>/vlasnik"><%=user.toString()%></a>
				</c:when>
				<c:when test="<%=user.getAuthLevel() == User.ADMIN%>">
					<a href="<%=request.getContextPath()%>/admin"><%=user.toString()%></a>
				</c:when>
				<c:otherwise>
					<%=user.toString()%>
				</c:otherwise>
			</c:choose></span>
	</p>

</div>
<div class="clearfix grpelem" id="u77-4">
	<!-- content -->
	<p>
		<a href="<%=request.getContextPath()%>/prijava?o=1">Odjava</a>
	</p>
</div>

<c:if test="<%=user.getAuthLevel() > User.MEMBER%>">
	<div class="clearfix grpelem" id="u618-6">
		<!-- content -->
		<p>
			Broj korisnika online:
			<%=UserCounter.getUsersOnline()%></p>
		<p>
			Admini online (<%=UserCounter.getNumberAdminsOnline()%>):
			<%=UserCounter.getAdminsOnline()%></p>
	</div>
</c:if>


<%
    } else {
%>

<div class="clearfix grpelem" id="u624-4">
	<!-- content -->
	<p>
		<a href="<%=request.getContextPath()%>/prijava">Prijava </a>
	</p>
</div>
<div class="clearfix grpelem" id="u625-4">
	<!-- content -->
	<p>
		<a href="<%=request.getContextPath()%>/registracija">Registracija
		</a>
	</p>
</div>

<%
    }
%>