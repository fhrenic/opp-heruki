<%@page import="hr.fer.zemris.opp.util.Utility"%>
<%@page import="web.servleti.UserCounter"%>
<%@page import="hr.fer.zemris.opp.model.User"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html class="html" lang="en-US">
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
<title>Pretraga</title>
<!-- CSS -->
<link rel="stylesheet" type="text/css"
	href="css/site_global.css?4052507572" />
<link rel="stylesheet" type="text/css"
	href="css/index_pretraga.css?267385253" id="pagesheet" />
</head>
<body>
	<div class="clearfix" id="page">
		<!-- column -->
		<div class="position_content" id="page_position_content">
			<div class="clearfix colelem" id="pu656">
				<!-- group -->
				<div class="clip_frame grpelem" id="u656">
					<!-- image -->
					<img class="block" id="u656_img" src="images/untitled-7.jpg" alt=""
						width="1280" height="400" />
				</div>

				<!-- TEKST NA SLICI -->

				<div class="clearfix grpelem" id="u87-6">
					<!-- content -->
					<p id="u87-2">APARTMANI HERUKI</p>
					<p id="u87-4">Rezervirajte!</p>
				</div>

				<jsp:include page="Header.jsp" />

			</div>


			<c:choose>
				<c:when test="${groups.isEmpty()}">
					<div class="clearfix colelem" id="u570-4">
						<!-- content -->
						<p>Nema rezultata za Vaš zahtjev</p>
					</div>
				</c:when>
				<c:otherwise>
					<div class="clearfix colelem" id="u570-4">
						<!-- content -->
						<p>Izaberite sobu</p>
					</div>
					<c:forEach var="g" items="${groups.keySet()}">
						<div class="clearfix colelem" id="pu576">
							<!-- group -->
							<div class="clip_frame grpelem" id="u576">
								<!-- image -->
								<img class="block" id="u576_img"
									src="images/sobe/${g.picture}.png" alt="" width="214"
									height="160" />
							</div>
							<div class="clearfix grpelem" id="pu663-4">
								<!-- column -->
								<div class="clearfix colelem" id="u663-4">
									<!-- content -->
									<p>
										<a id="blackcolor"
											href="<%=request.getContextPath()%>/ponuda?gid=${g.id}">${g.name}</a>
									</p>
								</div>
								<div class="clearfix colelem" id="u606-11">
									<!-- content -->
									<p>${g.description}</p>
									<p>Broj osoba ${g.capacityLow}-${g.capacityHigh}</p>
									<p>Cijena po danu ${g.price} kn</p>
									<p>Preostalo je ${groups[g]} slobodnih jedinica</p>
								</div>
							</div>
						</div>
					</c:forEach>
				</c:otherwise>
			</c:choose>
			<div class="verticalspacer"></div>
		</div>
	</div>
</body>
</html>
