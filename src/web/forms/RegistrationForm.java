package web.forms;

import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.opp.dao.DAOProvider;
import hr.fer.zemris.opp.model.Address;
import hr.fer.zemris.opp.model.Country;
import hr.fer.zemris.opp.model.User;

public class RegistrationForm extends Form {

    private String nickname = "";
    private String password = "";
    private String password2 = "";
    private String firstname = "";
    private String lastname = "";
    private String email = "";
    private String telephone = ""; // not obligatorys

    private String street = "";
    private String city = "";
    private Country country;

    @Override
    public void validate() {
        if (!password.equals(password2)) addError("password", "Lozinke se ne poklapaju");
    }

    @Override
    public void fillFromRequest(HttpServletRequest req) {
        nickname = getFromReq(req, "nickname");
        password = getFromReq(req, "password");
        password2 = getFromReq(req, "password2");
        firstname = getFromReq(req, "firstname");
        lastname = getFromReq(req, "lastname");

        email = getFromReq(req, "email");
        telephone = getFromReq(req, "telephone");

        street = getFromReq(req, "street");
        city = getFromReq(req, "city");
        country = DAOProvider.getDAO().find(Country.class,
                Long.parseLong(getFromReq(req, "country")));
    }

    @Override
    public void reset() {
        password = "";
        password2 = "";
    }

    public User fillUser() {

        // address
        Address address = new Address();
        address.setStreet(street);
        address.setCity(city);
        address.setCountry(country);

        // user
        User user = new User();
        user.setNickname(nickname);
        user.setPassword(password);
        user.setFirstname(firstname);
        user.setLastname(lastname);
        user.setEmail(email);
        user.setTelephone(telephone);
        user.setAddress(address);

        return user;
    }

    public String getNickname() {
        return nickname;
    }

    public String getPassword() {
        return password;
    }

    public String getPassword2() {
        return password;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public Country getCountry() {
        return country;
    }

}
