package web.forms;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.opp.util.Utility;

public class SearchForm extends Form {

    public static void main(String[] args) {
        SearchForm f = new SearchForm();
        System.out.println(f.dateTo.get(Calendar.MONTH));
        f.dateTo.set(Calendar.MONTH, Calendar.SEPTEMBER);
        System.out.println(f.dateTo.get(Calendar.MONTH));
    }

    private int adults;
    private int children;
    private String childrenAge;
    private Calendar dateFrom = Calendar.getInstance();
    private Calendar dateTo = Calendar.getInstance();;
    private String view;

    @Override
    public void validate() {
        Calendar today = Utility.today();
        String error = null;
        if (dateFrom.before(today)) error = "Datum od je prosao";
        else if (dateTo.before(today)) error = "Datum do je prosao";
        else if (dateFrom.after(dateTo)) error = "Datum od mora biti prije datuma do";
        else if (dateFrom.get(Calendar.YEAR) != dateTo.get(Calendar.YEAR))
            error = "Datumi moraju biti u istoj godini";
        else if (!Utility.inSeason(dateFrom, dateTo))
            error = "Moguce je rezervirati od 1.5. do 30.9.";

        if (error != null) addError("date", error);
    }

    @Override
    public void fillFromRequest(HttpServletRequest req) {
        adults = Integer.parseInt(req.getParameter("adults"));
        children = Integer.parseInt(req.getParameter("children"));
        childrenAge = req.getParameter("childrenAge");
        view = req.getParameter("view");
        Utility.setDate("dateFrom", req, dateFrom);
        Utility.setDate("dateTo", req, dateTo);
    }

    @Override
    public void reset() {
        // not needed
    }

    public int getAdults() {
        return adults;
    }

    public int getChildren() {
        return children;
    }

    public String getChildrenAge() {
        return childrenAge;
    }

    public Calendar getDateFrom() {
        return dateFrom;
    }

    public Calendar getDateTo() {
        return dateTo;
    }

    public String getView() {
        return view;
    }

}
