package web.forms;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * This class contains methods that every form has.
 * 
 * @author Filip Hrenić
 * @version 1.0
 */
public abstract class Form {

    // name of property, error
    protected Map<String, String> errors = new HashMap<>();

    /**
     * @return <code>true</code> if form contains errors
     */
    public boolean hasErrors() {
        return !errors.isEmpty();
    }

    /**
     * @param key tested field, will return true if there's an error for this property
     * @return <code>true</code> if there's an error
     */
    public boolean hasErrorFor(String key) {
        return errors.containsKey(key);
    }

    /**
     * @param key defines which error you want
     * @return error message
     */
    public String getErrorFor(String key) {
        return errors.get(key);
    }

    public void addError(String key, String error) {
        errors.put(key, error);
    }

    /**
     * Checks if this form has errors.
     */
    public abstract void validate();

    public abstract void fillFromRequest(HttpServletRequest req);

    public abstract void reset();

    protected String getFromReq(HttpServletRequest req, String param) {
        return editObject(req.getParameter(param));
    }

    /**
     * If parameters is <code>null</code> then return an empty string, otherwise returns a string
     * with no leading or trailing spaces.
     * 
     * @param o object to operate on
     * @return string 
     */
    private static String editObject(Object o) {
        if (o == null) {
            return "";
        }
        return o.toString().trim();
    }

}