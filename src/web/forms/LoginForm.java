package web.forms;

import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.opp.model.User;

public class LoginForm extends Form {

    private String nickname = "";
    private String password = "";

    @Override
    public void validate() {
    }

    @Override
    public void fillFromRequest(HttpServletRequest req) {
        nickname = getFromReq(req, "nickname");
        password = getFromReq(req, "password");
    }

    @Override
    public void reset() {
        password = "";
    }

    public User fillUser() {
        User user = new User();
        user.setNickname(nickname);
        user.setPassword(password);
        return user;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

}
