package web.servleti;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.opp.dao.DAO;
import hr.fer.zemris.opp.dao.DAOProvider;
import hr.fer.zemris.opp.model.User;
import hr.fer.zemris.opp.util.Utility;

@WebServlet("/vlasnik")
public class OwnerServlet extends AuthorizedServlet {

	private static final int MAX_ADMIN = 3;

	public OwnerServlet() {
		super(User.OWNER);
	}

	private static final long serialVersionUID = 6309718957984829456L;

	@Override
	protected void doGetCustom(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String nick = req.getParameter("adminNick");
		if (nick != null) {

			String error = null;
			DAO dao = DAOProvider.getDAO();
			int numberOfAdmins = dao.numberOfAdmins();

			if (numberOfAdmins == MAX_ADMIN)
				error = "Postoje " + MAX_ADMIN + " admina";
			else {
				User newAdmin = dao.getUser(nick);
				if (newAdmin == null)
					error = "Ne postoji korisnik s tim nadimkom";
				else if (newAdmin.getAuthLevel() != User.MEMBER)
					error = "Korisnik je admin/vlasnik";
				else {
					String num = Integer.toString(numberOfAdmins + 1);
					String body = "<html><head></head><body>Postovani korisnice,<br><br>"
							+ "Postavljeni ste za administratora.<br>"
							+ "Podaci za prijavu na sustav su nepromijenjeni.<br><br> "
							+ "Dodijeljen Vam je mail: admin"
							+ num
							+ "@heruki.com.<br>"
							+ "Podaci za prijavu na mail su:<br>Korisnicko ime: "
							+ "admin"
							+ num
							+ "<br>Lozinka: "
							+ "admin"
							+ num
							+ "123" + "<br><br>Vas Heruki</body></html>";
					String subject = "Administrator - Apartmani Heruki";
					Utility.sendMailToUser(newAdmin.getEmail(), subject, body);
					newAdmin.setAuthLevel(User.ADMIN);
					dao.save(newAdmin);
				}
			}

			if (error != null) {
				req.setAttribute("info", error);
				req.setAttribute("hasInfo", true);
			}
		}

		req.getRequestDispatcher("/pages/Owner.jsp").forward(req, resp);
	}

	@Override
	protected void doPostCustom(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String id = req.getParameter("id");
		if (id != null) {
			String parameters = "id=" + id;
			if ("1".equals(id)) {
				String from = req.getParameter("from");
				String to = req.getParameter("to");
				if (from != null)
					parameters += "&from=" + from;
				if (to != null)
					parameters += "&to=" + to;
			}
			req.setAttribute("hasParams", true);
			req.setAttribute("params", parameters);
		}
		req.getRequestDispatcher("/pages/Owner.jsp").forward(req, resp);
	}
}
