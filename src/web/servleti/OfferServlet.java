package web.servleti;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.opp.dao.DAOProvider;
import hr.fer.zemris.opp.model.Group;
import hr.fer.zemris.opp.model.Service;
import hr.fer.zemris.opp.model.User;
import hr.fer.zemris.opp.util.Utility;
import web.forms.SearchForm;

@WebServlet("/ponuda")
public class OfferServlet extends AuthorizedServlet {

    public OfferServlet() {
        super(User.GUEST);
    }

    private static final long serialVersionUID = -860072163148059822L;

    @Override
    protected void doGetCustom(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        SearchForm form = (SearchForm) req.getSession().getAttribute("form"); // FIXME
        Calendar dateFrom = form.getDateFrom();
        Calendar dateTo = form.getDateTo();
        long gid = (Long) Long.parseLong(req.getParameter("gid"));

        Group group = DAOProvider.getDAO().find(Group.class, gid);

        req.setAttribute("g", group);
        req.setAttribute("dateFrom", Utility.dateToString(dateFrom));
        req.setAttribute("dateTo", Utility.dateToString(dateTo));
        req.setAttribute("price", Utility.price(group, dateFrom, dateTo));
        req.setAttribute("loggedIn", req.getSession().getAttribute("current.user") != null);
        req.setAttribute("services", DAOProvider.getDAO().all(Service.class));

        req.getRequestDispatcher("/pages/Offer.jsp").forward(req, resp);

    }
}
