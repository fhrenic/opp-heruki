package web.servleti;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.opp.dao.DAOProvider;
import hr.fer.zemris.opp.model.Group;
import hr.fer.zemris.opp.model.User;
import web.forms.SearchForm;

@WebServlet("/pretraga")
public class SearchServlet extends AuthorizedServlet {

    private static final long serialVersionUID = -801963105247136666L;

    public SearchServlet() {
        super(User.GUEST);
    }

    @Override
    protected void doPostCustom(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        SearchForm form = new SearchForm();
        form.fillFromRequest(req);
        form.validate();

        if (form.hasErrors()) {
            req.setAttribute("form", form);
            req.getRequestDispatcher("/pages/Main.jsp").forward(req, resp);
            return;
        }

        Map<Group, Long> groups = DAOProvider.getDAO().groupsWithCounts(form);
        req.setAttribute("groups", groups);
        req.getSession().setAttribute("form", form); // FIXME
        req.getRequestDispatcher("/pages/Search.jsp").forward(req, resp);
    }

}
