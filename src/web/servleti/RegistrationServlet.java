package web.servleti;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.opp.dao.DAO;
import hr.fer.zemris.opp.dao.DAOProvider;
import hr.fer.zemris.opp.model.Country;
import hr.fer.zemris.opp.model.User;
import hr.fer.zemris.opp.util.Utility;
import web.forms.RegistrationForm;

@WebServlet("/registracija")
public class RegistrationServlet extends AuthorizedServlet {

    private static final long serialVersionUID = 984468117267965409L;

    public RegistrationServlet() {
        super(User.GUEST);
    }

    @Override
    protected void doGetCustom(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.setAttribute("form", new RegistrationForm());
        req.setAttribute("countries", DAOProvider.getDAO().all(Country.class));
        req.getRequestDispatcher("/pages/Registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPostCustom(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        // ovo se pozove kad se unosi u form

        RegistrationForm form = new RegistrationForm();
        form.fillFromRequest(req);
        form.validate();

        DAO dao = DAOProvider.getDAO();
        User user = form.fillUser();

        if (dao.userHasNickname(user.getNickname()))
            form.addError("nickname", "Nadimak je zauzet, izaberite neki drugi");
        else if (dao.userHasEmail(user.getEmail()))
            form.addError("email", "Mail adresa se vec koristi");

        if (form.hasErrors()) {
            form.reset();
            req.setAttribute("form", form);
            req.setAttribute("countries", DAOProvider.getDAO().all(Country.class));
            req.getRequestDispatcher("/pages/Registration.jsp").forward(req, resp);
            return;
        }

        int authLevel = User.MEMBER;
        if (dao.noUsers()) authLevel = User.OWNER;

        user.setAuthLevel(authLevel);
        dao.saveUser(user);

        // send mail
        String link = req.getScheme() + "://" + req.getServerName()
                + req.getServletContext().getContextPath() + "/prijava?nick=" + user.getNickname();

        String body = "<html><head></head><body>Postovani korisnice,<br><br>"
                + "Vasa registracija je uspjesno obavljena.<br>"
                + "Za prijavu u sustav idite na <a href=\"" + link + "\">ovu poveznicu</a><br><br>"
                + "Podaci za prijavu su:<br>Korisnicko ime: " + user.getNickname() + "<br>Lozinka: "
                + form.getPassword() + "<br><br>Vas Heruki</body></html>";
        String subject = "Dobrodosli - Apartmani Heruki";

        Utility.sendMailToUser(user.getEmail(), subject, body);

        // registration success

        String heading = "Uspjesna registracija!";
        String paragraph = "Dragi <b>" + user.getFirstname() + " " + user.getLastname()
                + "</b>, tvoja registracija bila je uspjesna!<br>" + "Na " + user.getEmail()
                + " poslana je pozdravna poruka i upute za prvu prijavu.";

        req.setAttribute("infoHeading", heading);
        req.setAttribute("infoParagraph", paragraph);
        req.getRequestDispatcher("/pages/Info.jsp").forward(req, resp);
    }

}
