package web.servleti;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.opp.dao.DAO;
import hr.fer.zemris.opp.dao.DAOProvider;
import hr.fer.zemris.opp.model.User;
import web.forms.LoginForm;

/**
*
* @author fhrenic
*/
@WebServlet("/prijava")
public class LoginServlet extends AuthorizedServlet {

    private static final long serialVersionUID = 6353278214464543740L;

    public LoginServlet() {
        super(User.GUEST);
    }

    @Override
    protected void doGetCustom(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        // ovo se pozove kad se netko hoce logirati

        String odjava = req.getParameter("o");
        if (odjava != null) {
            UserCounter.removeUser(req.getSession());
            resp.sendRedirect(req.getServletContext().getContextPath() + "/");
            return;
        }

        LoginForm form = new LoginForm();
        String nickname = req.getParameter("nick");
        if (nickname != null) form.setNickname(nickname);

        req.setAttribute("form", form);
        req.getRequestDispatcher("/pages/Login.jsp").forward(req, resp);
    }

    @Override
    protected void doPostCustom(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        // ovo se pozove kad se unosi u form

        LoginForm form = new LoginForm();
        form.fillFromRequest(req);

        DAO dao = DAOProvider.getDAO();

        User user = form.fillUser();

        if (dao.getUser(user.getNickname()) == null)
            form.addError("nickname", "Ne postoji korisnik s tim imenom");
        else {
            user = dao.getUser(user.getNickname(), user.getPassword());
            if (user == null) form.addError("password", "Pogresna sifra");
        }

        if (form.hasErrors()) {
            form.reset();
            req.setAttribute("form", form);
            req.getRequestDispatcher("/pages/Login.jsp").forward(req, resp);
            return;
        }

        if (user.getAuthLevel() == User.ADMIN) UserCounter.addAdmin(user);

        req.getSession().setAttribute("current.user", user);
        resp.sendRedirect(req.getContextPath() + "/");
    }

}
