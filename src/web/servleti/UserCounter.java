package web.servleti;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import hr.fer.zemris.opp.model.User;

@WebListener
public class UserCounter implements HttpSessionListener {

    private static final AtomicInteger sessionCounter = new AtomicInteger(0);
    private static List<User> admins = new ArrayList<>(3);

    @Override
    public void sessionCreated(HttpSessionEvent arg0) {
        sessionCounter.incrementAndGet();
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent arg0) {
        removeUser(arg0.getSession());
        if (sessionCounter.get() > 0) sessionCounter.decrementAndGet();
        if (sessionCounter.get() == 0) admins.clear();
    }

    public static void removeUser(HttpSession session) {
        Object oUser = session.getAttribute("current.user");
        if (oUser != null) {
            User user = (User) oUser;
            if (user.getAuthLevel() == User.ADMIN) admins.remove(user);
        }
        session.removeAttribute("current.user");
    }

    public static void addAdmin(User admin) {
        admins.add(admin);
    }

    public static String getUsersOnline() {
        return sessionCounter.toString();
    }

    public static String getAdminsOnline() {
        if (admins.isEmpty()) return "";
        String str = admins.get(0).toString();
        for (int i = 1; i < admins.size(); i++)
            str += ", " + admins.get(i).toString();
        return str;
    }

    public static int getNumberAdminsOnline() {
        return admins.size();
    }

}
