package web.servleti;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.opp.dao.DAOProvider;
import hr.fer.zemris.opp.model.Accomodation;
import hr.fer.zemris.opp.model.Group;
import hr.fer.zemris.opp.model.Reservation;
import hr.fer.zemris.opp.model.Service;
import hr.fer.zemris.opp.model.User;
import hr.fer.zemris.opp.util.Utility;
import web.forms.SearchForm;

@WebServlet("/rezervacija")
public class ReservationServlet extends AuthorizedServlet {

    public ReservationServlet() {
        super(User.MEMBER);
    }

    private static final long serialVersionUID = -829341054825784612L;

    @Override
    protected void doPostCustom(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        SearchForm form = (SearchForm) req.getSession().getAttribute("form");
        req.getSession().removeAttribute("form");
        long gid = Long.parseLong(req.getParameter("gid"));

        User user = (User) req.getSession().getAttribute("current.user");
        Group group = DAOProvider.getDAO().find(Group.class, gid);
        Accomodation acc = DAOProvider.getDAO().optimalAccomodation(group, form.getDateFrom(),
                form.getDateTo());

        List<Service> services = new LinkedList<>();
        if (req.getParameterValues("s") != null) {
            for (String s : req.getParameterValues("s"))
                services.add(DAOProvider.getDAO().find(Service.class, Long.parseLong(s)));
        }

        Reservation r = new Reservation();
        r.setAccomodation(acc);
        r.setUser(user);
        r.setFromDate(form.getDateFrom());
        r.setToDate(form.getDateTo());
        r.setPrice(Utility.price(group, form.getDateFrom(), form.getDateTo()));
        r.setNumberOfAdults(form.getAdults());
        r.setNumberOfChildren(form.getChildren());
        r.setChildrenAge(form.getChildrenAge());
        r.setAdditionalServices(services);
        r.setReservationStatus(Reservation.RESERVATION_WAITING);

        DAOProvider.getDAO().save(r);

        // Send email

        String reservation = "Rezervacija no." + r.getId() + "<ul><li>Napravljena dana: "
                + Utility.dateToString(r.getMadeOn()) + "</li>" + "<li>Vrsta smjestaja: "
                + r.getAccomodation() + "</li>" + "<li>Od " + Utility.dateToString(r.getFromDate())
                + " do " + Utility.dateToString(r.getToDate()) + "</li>" + "<li>Broj odraslih: "
                + r.getNumberOfAdults() + "</lit>" + "<li>Broj djece: " + r.getNumberOfChildren()
                + " dobi " + r.getChildrenAge() + "</li>" + "<li>Status rezervacije: "
                + r.getConfirmedStatus() + "</li>";
        if (r.getHasServices()) {
            reservation += "<li>Zatrazene dodatne usluge<br><ol>";
            for (Service s : r.getAdditionalServices()) {
                reservation += "<li>" + s + "</li>";
            }
            reservation += "</ol></li>";
        }
        reservation += "</ul>";

        String body = "<html><body>Postovani " + user.getNickname()
                + ",<br><br>Uspjesno ste napravili rezervaciju:<br>" + reservation
                + "<br>U roku od 3 dana javit ce Vam se vlasnik turistickog naselja."
                + "<br><br>Vas Heruki</body></html>";

        Utility.sendMailToUser(user.getEmail(), "Rezervacija " + r.getId(), body);

        String heading = "Uspjesna rezervacija!";

        req.setAttribute("infoHeading", heading);
        req.setAttribute("infoParagraph", reservation);
        req.getRequestDispatcher("/pages/Info.jsp").forward(req, resp);
    }

}
