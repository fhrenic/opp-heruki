package web.servleti;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.opp.dao.DAO;
import hr.fer.zemris.opp.dao.DAOProvider;
import hr.fer.zemris.opp.model.User;

@WebServlet("/admin")
public class AdminServlet extends AuthorizedServlet {

    private static final long serialVersionUID = 8718024653782825503L;

    public AdminServlet() {
        super(User.ADMIN);
    }

    @Override
    protected void doGetCustom(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.getRequestDispatcher("/pages/Admin.jsp").forward(req, resp);
    }

    @Override
    protected void doPostCustom(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        DAO dao = DAOProvider.getDAO();
        String p = req.getParameter("potvrdi");
        if (p != null) dao.confirmReservation(Long.parseLong(p));
        else dao.removeReservation(Long.parseLong(req.getParameter("izbrisi")));

        req.setAttribute("hasInfo", true);
        req.setAttribute("info", "Uspjesno obavljeno");
        req.getRequestDispatcher("/pages/Admin.jsp").forward(req, resp);

        // TODO
    }

}
