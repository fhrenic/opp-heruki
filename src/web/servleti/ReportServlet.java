package web.servleti;

import java.io.IOException;
import java.util.Calendar;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.opp.dao.DAO;
import hr.fer.zemris.opp.dao.DAOProvider;
import hr.fer.zemris.opp.model.User;
import hr.fer.zemris.opp.util.Reports;
import hr.fer.zemris.opp.util.Utility;

@WebServlet("/report")
public class ReportServlet extends AuthorizedServlet {

    public static final int GROUP_ID = 1;
    public static final int SERVICE_ID = 2;
    public static final int CITY_ID = 3;
    public static final int COUNTRY_ID = 4;

    private static final long serialVersionUID = 8125216308604546996L;

    public ReportServlet() {
        super(User.OWNER);
    }

    @Override
    protected void doGetCustom(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        int type = Integer.parseInt(req.getParameter("t"));
        int id = Integer.parseInt(req.getParameter("id"));

        DAO dao = DAOProvider.getDAO();

        String name;
        Map<? extends Object, Long> data;

        if (id == GROUP_ID) {
            Calendar from = Calendar.getInstance();
            Calendar to = Calendar.getInstance();
            Utility.setDate("from", req, from);
            Utility.setDate("to", req, to);

            name = "Smjestajne jedinice";
            data = dao.groupsRank(from, to);
        } else if (id == SERVICE_ID) {
            name = "Usluge";
            data = dao.serviceRank();
        } else if (id == CITY_ID) {
            name = "Gradovi";
            data = dao.cityRank();
        } else {
            name = "Drzave";
            data = dao.countryRank();
        }

        if (type == Reports.CHART) resp.setContentType("image/png");
        else {
            resp.setContentType("application/octet-stream");
            resp.setHeader("Content-disposition", "attachment;filename=" + name + ".xls");
        }

        Reports.generate(type, name, data, resp.getOutputStream());
    }

}
