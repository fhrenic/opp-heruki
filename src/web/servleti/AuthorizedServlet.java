package web.servleti;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.opp.model.User;

public class AuthorizedServlet extends HttpServlet {

    private static final long serialVersionUID = -3545696660183135564L;

    private int authLevel;

    private Handler getHandler;
    private Handler postHandler;

    public AuthorizedServlet(int authLevel) {
        this.authLevel = authLevel;
        getHandler = this::doGetCustom;
        postHandler = this::doPostCustom;
    }

    private boolean checkAuthLevel(HttpServletRequest req) {
        if (authLevel == User.GUEST) return true;
        Object o = req.getSession().getAttribute("current.user");
        if (o == null) return false;
        User user = (User) o;
        return user.getAuthLevel() >= authLevel;
    }

    private void handle(HttpServletRequest req, HttpServletResponse resp, Handler handler)
            throws ServletException, IOException {

        String heading;
        String paragraph;

        if (checkAuthLevel(req)) {
            try {
                handler.handle(req, resp);
                return;
            } catch (Exception e) {
                heading = "Nepoznata pogreska";
                paragraph = "Dogodila se pogreska prilikom pristupa.<br>Molimo pokusajte ponovno.";
            }
        } else {
            heading = "Nedovoljne ovlasti";
            paragraph = "Pokusali ste pristupiti stranici kojoj nemate ovlasti pristupiti.<br>"
                    + "Ako mislite da biste trebali moci, javite se bilo kojem administratoru na mail.";
        }

        req.setAttribute("infoHeading", heading);
        req.setAttribute("infoParagraph", paragraph);
        req.getRequestDispatcher("/pages/Info.jsp").forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        handle(req, resp, getHandler);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        handle(req, resp, postHandler);
    }

    protected void doGetCustom(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
    }

    protected void doPostCustom(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
    }

    private static interface Handler {
        public void handle(HttpServletRequest req, HttpServletResponse resp)
                throws ServletException, IOException;
    }

}
