package web.servleti;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.opp.model.User;

/**
 *
 * @author fhrenic
 */
@WebServlet({ "/index.html" })
public class IndexServlet extends AuthorizedServlet {

    private static final long serialVersionUID = 2488717707187713873L;

    public IndexServlet() {
        super(User.GUEST);
    }

    @Override
    protected void doGetCustom(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.getRequestDispatcher("/pages/Main.jsp").forward(req, resp);
    }

}
