package hr.fer.zemris.opp.dao.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import hr.fer.zemris.opp.dao.DAOException;

/**
 * This class provides an Entity Manager to each thread. Ever thread has it's
 * own manager when it calls {@link #start()}.
 * 
 * @author fhrenic
 */
public class JPAHelper {

    /**
     * Get the entity manager that belongs to the current thread. If such
     * manager doesn't exist, one is created and <i>assigned</i> to this thread.
     * 
     * @return entity manager
     */
    public static EntityManager start() {
        EntityManager em = JPAEMFProvider.getFactory().createEntityManager();
        em.getTransaction().begin();
        return em;
    }

    /**
     * Commits the current transaction (or rolls back) and closes the entity
     * manager.
     * 
     * @throws DAOException if there is an error while committing/rolling
     *             back/closing
     */
    public static void close(EntityManager em) throws DAOException {
        DAOException dex = null;
        try {
            EntityTransaction transaction = em.getTransaction();
            if (transaction.isActive()) {
                if (!transaction.getRollbackOnly()) transaction.commit();
                else transaction.rollback();
            }
        } catch (Exception e) {
            dex = new DAOException("Unable to end the transaction.", e);
        }
        try {
            em.close();
        } catch (Exception e) {
            if (dex != null) {
                dex = new DAOException("Unable to close entity manager.", e);
            }
        }
        if (dex != null) throw dex;
    }

    /**
     * Singleton, static methods only
     */
    private JPAHelper() {
    }

}
