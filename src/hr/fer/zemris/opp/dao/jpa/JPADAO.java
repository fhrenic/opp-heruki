package hr.fer.zemris.opp.dao.jpa;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import hr.fer.zemris.opp.dao.DAO;
import hr.fer.zemris.opp.model.Accomodation;
import hr.fer.zemris.opp.model.Address;
import hr.fer.zemris.opp.model.Country;
import hr.fer.zemris.opp.model.Group;
import hr.fer.zemris.opp.model.Identificable;
import hr.fer.zemris.opp.model.Reservation;
import hr.fer.zemris.opp.model.Service;
import hr.fer.zemris.opp.model.User;
import hr.fer.zemris.opp.util.Props;
import hr.fer.zemris.opp.util.Utility;
import web.forms.SearchForm;

/**
 * An implementation of {@link DAO} that uses JPA specification and Entity
 * Managers.
 * 
 * @author fhrenic
 */
public class JPADAO implements DAO {

    public static void main(String[] args) {

        String factory = Props.get("factory");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory(factory);
        JPAEMFProvider.setFactory(emf);

        try {
            JPADAO dao = new JPADAO();

            Group g = dao.find(Group.class, 5);
            System.out.println(g);
            Calendar c = Calendar.getInstance();

            dao.optimalAccomodation(g, c, c);

        } catch (Exception e) {
            System.out.println(e);
        }

        emf.close();

    }

    private EntityManager em;

    private void init() {
        em = JPAHelper.start();
    }

    private void destroy() {
        JPAHelper.close(em);
        em = null;
    }

    @Override
    public User getUser(String nickname, String password) {
        init();
        List<User> users = em
                .createQuery(
                        "select u from User as u where u.nickname=:nickname and u.password=:password",
                        User.class)
                .setParameter("nickname", nickname).setParameter("password", password)
                .getResultList();
        if (users.isEmpty()) {
            return null;
        }
        destroy();
        return users.get(0);
    }

    @Override
    public User getUser(String nickname) {
        init();
        String query = "SELECT u FROM User AS u WHERE u.nickname = :nickname";
        List<User> user = em.createQuery(query, User.class).setParameter("nickname", nickname)
                .getResultList();
        destroy();
        if (user.isEmpty()) return null;
        return user.get(0);
    }

    @Override
    public int numberOfAdmins() {
        init();
        int admins = em.createQuery("select u from User as u where u.authLevel = :level")
                .setParameter("level", User.ADMIN).getResultList().size();
        destroy();
        return admins;
    }

    @Override
    public boolean userHasNickname(String nickname) {
        return userHas("nickname", nickname);
    }

    @Override
    public boolean userHasEmail(String email) {
        return userHas("email", email);
    }

    private boolean userHas(String propertyName, String property) {
        init();
        String query = "SELECT u FROM User AS u WHERE u." + propertyName + "=:" + propertyName;
        List<User> users = em.createQuery(query, User.class).setParameter(propertyName, property)
                .getResultList();
        if (users.isEmpty()) {
            return false;
        }
        destroy();
        return true;
    }

    @Override
    public void saveUser(User user) {
        init();
        Address address = user.getAddress();
        saveEntity(address);
        saveEntity(user);
        destroy();
    }

    @Override
    public boolean noUsers() {
        return em.createQuery("SELECT u FROM User u", User.class).setMaxResults(1).getResultList()
                .isEmpty();
    }

    @Override
    public Map<Group, Long> groupsWithCounts(SearchForm form) {

        String query = "SELECT g.id, COUNT(DISTINCT a.id) "
                + "FROM Reservation AS r RIGHT JOIN r.accomodation AS a RIGHT JOIN a.group AS g "
                + " WHERE (g.capacityHigh >= :people) AND (g.viewType = :view) AND "
                + "( (r.id IS NULL) OR (r.toDate < :from) OR (r.fromDate > :to) ) "
                + "GROUP BY g.id";

        int people = form.getAdults() + form.getChildren();

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("people", people);
        parameters.put("from", form.getDateFrom());
        parameters.put("to", form.getDateTo());
        parameters.put("view", form.getView());

        return count(parameters, query, Group.class);
    }

    @Override
    public Accomodation optimalAccomodation(Group group, Calendar from, Calendar to) {
        // 3. moguca slucaja, poredani od najoptimalnijeg do najgoreg
        //      (ako nema takve da zodovoljava uvjet x, vrati onu iz uvjeta x+1)
        //      1) nasli smo rezervaciju na koju bi se ova nastavila
        //      2) vrati smjestaj tako da rupa bude minimalna
        //      3) ovo se dogodi kad je sa svim proslim rezervacijama kolizija ili jos 
        //          nema ni jedne rezervacije i ostaju samo prazni, onda vracam prazni

        init();

        // filter out overlapping
        String queryOverlap = "SELECT a FROM Reservation AS r JOIN r.accomodation AS a "
                + "WHERE (a.group = :group) AND ((r.toDate >= :from) OR (r.fromDate <= :to))";
        List<Accomodation> overlap = em.createQuery(queryOverlap, Accomodation.class)
                .setParameter("group", group).setParameter("from", from).setParameter("to", to)
                .getResultList();

        // select accommodations with accompanying reservations
        String queryFilter = "SELECT r, a FROM Reservation AS r RIGHT JOIN r.accomodation AS a"
                + " WHERE (a.group = :group)";
        if (!overlap.isEmpty()) queryFilter += " AND (a NOT IN :overlap)";
        TypedQuery<Object[]> query = em.createQuery(queryFilter, Object[].class)
                .setParameter("group", group);
        if (!overlap.isEmpty()) query.setParameter("overlap", overlap);
        List<Object[]> filtered = query.getResultList();

        Accomodation optimal = null;
        Accomodation emptyAcc = null;
        long rupa = Long.MAX_VALUE;
        long minRupa = 1;

        for (Object[] result : filtered) {
            Reservation r = (Reservation) result[0];
            Accomodation a = (Accomodation) result[1];

            if (r == null) { // no reservations for this accomodation
                if (emptyAcc == null) emptyAcc = a;
            } else {
                long trenutnaRupa = getRupa(r, from, to);
                if (trenutnaRupa <= minRupa) return a; // found optimal
                if (trenutnaRupa < rupa) { // new minimum
                    rupa = trenutnaRupa;
                    optimal = a;
                }
            }
        }
        destroy();
        return optimal != null ? optimal : emptyAcc;
    }

    private long getRupa(Reservation r, Calendar from, Calendar to) {
        if (r.getFromDate().after(to)) return Utility.daysBetween(to, r.getFromDate());
        else return Utility.daysBetween(from, r.getToDate());
    }

    @Override
    public <T> T find(Class<T> c, long id) {
        init();
        T t = em.find(c, id);
        destroy();
        return t;
    }

    @Override
    public <T> List<T> all(Class<T> c) {
        init();
        String className = c.getSimpleName();
        List<T> list = em.createQuery("select t from " + className + " t", c).getResultList();
        destroy();
        return list;
    }

    public void save(Identificable entity) {
        init();
        saveEntity(entity);
        destroy();
    }

    private void saveEntity(Identificable entity) {
        if (entity.getId() == null) {
            em.persist(entity); // create
        } else {
            em.merge(entity); // update
        }
    }

    // ranks

    @Override
    public Map<String, Long> cityRank() {
        init();
        String query = "SELECT A.city, COUNT(R.id) "
                + "FROM Reservation AS R JOIN R.user AS U JOIN U.address AS A GROUP BY A.city";

        List<Object[]> resultList = em.createQuery(query, Object[].class).getResultList();

        if (resultList.isEmpty()) {
            destroy();
            return new HashMap<>();
        }

        Map<String, Long> resultMap = new HashMap<>(resultList.size());
        for (Object[] result : resultList)
            resultMap.put((String) result[0], (Long) result[1]);

        destroy();
        return resultMap;
    }

    @Override
    public Map<Service, Long> serviceRank() {
        //String query = "SELECT S.id, COALESCE(SUM(R.numberOfAdults + R.numberOfChildren),0)"
        String query = "SELECT S.id, COUNT(R.id)"
                + " FROM Reservation AS R RIGHT JOIN R.additionalServices as S GROUP BY S.id";
        return count(null, query, Service.class);
    }

    @Override
    public Map<Group, Long> groupsRank(Calendar from, Calendar to) {
        String query = " SELECT G.id, COUNT(R.id) FROM Reservation AS R "
                + " RIGHT JOIN R.accomodation AS A RIGHT JOIN A.group AS G "
                + " WHERE (R.id IS NULL) OR (R.toDate < :to AND R.fromDate > :from) "
                + " GROUP BY G.id";
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("from", from);
        parameters.put("to", to);
        return count(parameters, query, Group.class);
    }

    @Override
    public Map<Country, Long> countryRank() {
        String query = "SELECT C.id, COUNT(R.id) "
                + "FROM Reservation AS R JOIN R.user AS U JOIN U.address AS A RIGHT JOIN A.country AS C "
                + "GROUP BY C.id";
        return count(null, query, Country.class);
    }

    private <T extends Identificable> Map<T, Long> count(Map<String, Object> parameters,
            String query, Class<T> c) {
        init();
        TypedQuery<Object[]> tq = em.createQuery(query, Object[].class);
        if (parameters != null) {
            for (String key : parameters.keySet())
                tq.setParameter(key, parameters.get(key));
        }

        List<Object[]> resultList = tq.getResultList();

        if (resultList.isEmpty()) {
            destroy();
            return new HashMap<>();
        }

        Map<Long, Long> idCountMap = new HashMap<>(resultList.size());
        for (Object[] result : resultList)
            idCountMap.put((Long) result[0], (Long) result[1]);

        List<T> ts = em
                .createQuery("SELECT e FROM " + c.getSimpleName() + " AS e WHERE e.id IN :ids", c)
                .setParameter("ids", idCountMap.keySet()).getResultList();

        Map<T, Long> resultMap = new HashMap<>(ts.size());
        for (T t : ts)
            resultMap.put(t, idCountMap.get(t.getId()));

        destroy();
        return resultMap;
    }

    @Override
    public void removeReservation(Long id) {
        Reservation r = find(Reservation.class, id);
        if (r != null) {
            init();
            em.remove(r);
            destroy();
        }
    }

    @Override
    public void confirmReservation(Long id) {
        Reservation r = find(Reservation.class, id);
        if (r != null) {
            switch (r.getReservationStatus()) {
                case (Reservation.RESERVATION_CONFIRMED):
                    // nothing
                    break;
                case (Reservation.RESERVATION_DECLINED):
                    // nothing, already declined
                    break;
                case (Reservation.RESERVATION_WAITING):
                    if (Utility.daysBetween(r.getMadeOn(), Utility.today()) > 3) em.remove(r);
                    else {
                        String subject = "Potvrda rezervacije";
                        String body = "<html><body>" + "Postovani,<br><br>Vasa rezervacija no."
                                + r.getId() + " je potvrdena.<br><br>Vas Heruki" + "</body></html>";

                        Utility.sendMailToUser(r.getUser().getEmail(), subject, body);
                        r.setReservationStatus(Reservation.RESERVATION_CONFIRMED);
                        save(r);
                    }
                    break;
                default:
                    break;
            }
        }

    }

}
