package hr.fer.zemris.opp.dao.jpa;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import hr.fer.zemris.opp.dao.DAOProvider;
import hr.fer.zemris.opp.model.User;
import hr.fer.zemris.opp.util.Parser;
import hr.fer.zemris.opp.util.Props;
import hr.fer.zemris.opp.util.Utility;

/**
 * This listener has methods that are invoked when the application starts and
 * ends. It is used to create and destroy the Entity Manager Factory.
 * 
 * @author fhrenic
 */
@WebListener
public class JPAInitListener implements ServletContextListener {

    public static void main(String[] args) {
        JPAInitListener x = new JPAInitListener();

        User u = new User();
        u.setPassword("hrenic");
        u.setNickname("hrenic");

        x.contextInitialized(null);

        System.out.println(DAOProvider.getDAO().getUser(u.getNickname(), u.getPassword()));
        System.out.println(u.getPassword());
        System.out.println(Utility.getEncoded("hrenic"));

        x.contextDestroyed(null);
    }

    /**
     * Creates a factory and sets it to the {@link JPAEMFProvider}.
     * 
     * @param sce not used
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        String factory = Props.get("factory");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory(factory);
        JPAEMFProvider.setFactory(emf);

        // XXX
        Parser.loadAndSave();

    }

    /**
     * Closes the entity manager factory that was started with the application.
     * 
     * @param sce not used
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        JPAEMFProvider.getFactory().close();
        JPAEMFProvider.setFactory(null);
    }

}