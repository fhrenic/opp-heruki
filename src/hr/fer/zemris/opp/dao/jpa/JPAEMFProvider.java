package hr.fer.zemris.opp.dao.jpa;

import javax.persistence.EntityManagerFactory;

/**
 * This class is only a provider of an Entity Manager Factory. That factory is
 * then used for creating Entity Managers, of course.
 * 
 * @author fhrenic
 */
public class JPAEMFProvider {

    /**
     * Currently used factory.
     */
    protected static EntityManagerFactory factory;

    /**
     * @return currently used factory (can be <code>null</code> if not set)
     */
    public static EntityManagerFactory getFactory() {
        return factory;
    }

    /**
     * Sets a new factory that will create entity managers
     * 
     * @param factory new factory
     */
    public static void setFactory(EntityManagerFactory factory) {
        JPAEMFProvider.factory = factory;
    }

    /**
     * Singleton, static methods only
     */
    private JPAEMFProvider() {
    }
}
