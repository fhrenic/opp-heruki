package hr.fer.zemris.opp.dao.jpa;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import hr.fer.zemris.opp.util.Utility;

/**
 * This filter is called on every page. It only calls close on entity manager
 * when you exit a page so that the transaction could be commited.
 * 
 * @author fhrenic
 */
@WebFilter("/*")
public class JPAFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        request.setCharacterEncoding(Utility.ENCODING);
        response.setCharacterEncoding(Utility.ENCODING);
        response.setContentType("text/html");
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }

}
