package hr.fer.zemris.opp.dao;

import hr.fer.zemris.opp.util.Props;

/**
 * This class provides the used {@link DAO}. It is loaded from the project
 * properties file.
 * 
 * @author fhrenic
 */
public class DAOProvider {

    /**
     * {@link DAO} being used by the whole application.
     */
    private static final DAO USED_DAO;

    static {
        String usedDao = Props.get("dao");
        Class<?> referenceToClass;
        Object dao = null;

        try {
            referenceToClass = DAOProvider.class.getClassLoader().loadClass(usedDao);
            dao = referenceToClass.newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            throw new DAOException("Can't load dao at location " + usedDao, e);
        }

        USED_DAO = (DAO) dao;
    }

    /**
     * Returns the used data access object.
     * 
     * @return dao
     */
    public static DAO getDAO() {
        return USED_DAO;
    }

    /**
     * Singleton, static methods only
     */
    private DAOProvider() {
    }

}