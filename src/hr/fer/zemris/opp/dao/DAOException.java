package hr.fer.zemris.opp.dao;


/**
 * Exception that is used in the data access object, when something goes wrong.
 * 
 * @author fhrenic
 */
public class DAOException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new dao exception with {@code null} as its detail message.
     */
    public DAOException() {
        super();
    }

    /**
     * Constructs a new dao exception with the specified detail message.
     *
     * @param message the detail message
     */
    public DAOException(String message) {
        super(message);
    }

    /**
     * Constructs a new dao exception with the specified cause and a detail
     * message of <tt>(cause==null ? null : cause.toString())</tt> (which
     * typically contains the class and detail message of <tt>cause</tt>)
     *
     * @param cause the cause (a <tt>null</tt> value is permitted, and indicates
     *            that the cause is nonexistent or unknown)
     */
    public DAOException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new dao exception with the specified detail message and
     * cause. Note that the detail message associated with {@code cause} is
     * <i>not</i> automatically incorporated in this runtime exception's detail
     * message.
     *
     * @param message the detail message
     * @param cause the cause (a <tt>null</tt> value is permitted, and indicates
     *            that the cause is nonexistent or unknown)
     */
    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

}