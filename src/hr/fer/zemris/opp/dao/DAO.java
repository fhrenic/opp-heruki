package hr.fer.zemris.opp.dao;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import hr.fer.zemris.opp.model.Accomodation;
import hr.fer.zemris.opp.model.Country;
import hr.fer.zemris.opp.model.Group;
import hr.fer.zemris.opp.model.Identificable;
import hr.fer.zemris.opp.model.Service;
import hr.fer.zemris.opp.model.User;
import web.forms.SearchForm;

/**
 * Data Access Object, provides an abstract interface to some persistence
 * mechanism. Used for handling data.
 * 
 * @author fhrenic
 */
public interface DAO {

    public User getUser(String nickname, String password);

    public User getUser(String nickname);

    public int numberOfAdmins();

    public boolean userHasNickname(String nickname);

    public boolean userHasEmail(String email);

    public void saveUser(User user);

    public boolean noUsers();

    public Map<Group, Long> groupsWithCounts(SearchForm form);

    // za danu grupu, datum od-do, nadji optimalni accomodation
    public Accomodation optimalAccomodation(Group group, Calendar from, Calendar to);

    public <T> T find(Class<T> c, long id);

    public <T> List<T> all(Class<T> c);

    public void save(Identificable i);

    // ranks

    public Map<String, Long> cityRank();

    public Map<Country, Long> countryRank();

    public Map<Service, Long> serviceRank();

    public Map<Group, Long> groupsRank(Calendar from, Calendar to);

    // reservations

    public void removeReservation(Long id);

    public void confirmReservation(Long id);

}
