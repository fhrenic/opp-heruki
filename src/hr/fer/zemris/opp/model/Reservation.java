package hr.fer.zemris.opp.model;

import java.util.Calendar;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author fhrenic
 */
@Entity
@Table(name = "RESERVATIONS")
public class Reservation extends Identificable {

    public static final int RESERVATION_DECLINED = 0;
    public static final int RESERVATION_CONFIRMED = 1;
    public static final int RESERVATION_WAITING = 2;

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Accomodation accomodation;

    @ManyToOne
    private User user;

    @Temporal(TemporalType.DATE)
    private Calendar madeOn;

    @Temporal(TemporalType.DATE)
    private Calendar confirmedOn;

    @Temporal(TemporalType.DATE)
    private Calendar fromDate;

    @Temporal(TemporalType.DATE)
    private Calendar toDate;

    @Column(nullable = false)
    private Integer numberOfAdults;

    @Column(nullable = false)
    private Integer numberOfChildren;

    @Column(nullable = false, length = 6)
    private String childrenAge; // i.e. 0-3

    @ManyToMany(fetch = FetchType.EAGER)
    private Collection<Service> additionalServices;

    @Column(nullable = false, scale = 2) // 2 decimal places
    private Double price;

    @Column(nullable = false)
    private Integer reservationStatus; // i.e. confirmed, declined, waiting

    public Reservation() {
        madeOn = Calendar.getInstance();
    }

    public String getConfirmedStatus() {
        if (reservationStatus == RESERVATION_DECLINED) return "odbijena";
        if (reservationStatus == RESERVATION_CONFIRMED) return "potvrdena";
        if (reservationStatus == RESERVATION_WAITING) return "ceka se potvrda";
        return "****";
    }

    public boolean getHasServices() {
        return !additionalServices.isEmpty();
    }

    // #################################################################################

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the accomodation
     */
    public Accomodation getAccomodation() {
        return accomodation;
    }

    /**
     * @param accomodation the accomodation to set
     */
    public void setAccomodation(Accomodation accomodation) {
        this.accomodation = accomodation;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the fromDate
     */
    public Calendar getFromDate() {
        return fromDate;
    }

    /**
     * @param fromDate the fromDate to set
     */
    public void setFromDate(Calendar fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * @return the toDate
     */
    public Calendar getToDate() {
        return toDate;
    }

    /**
     * @param toDate the toDate to set
     */
    public void setToDate(Calendar toDate) {
        this.toDate = toDate;
    }

    /**
     * @return the numberOfAdults
     */
    public Integer getNumberOfAdults() {
        return numberOfAdults;
    }

    /**
     * @param numberOfAdults the numberOfAdults to set
     */
    public void setNumberOfAdults(Integer numberOfAdults) {
        this.numberOfAdults = numberOfAdults;
    }

    /**
     * @return the numberOfChildren
     */
    public Integer getNumberOfChildren() {
        return numberOfChildren;
    }

    /**
     * @param numberOfChildren the numberOfChildren to set
     */
    public void setNumberOfChildren(Integer numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

    /**
     * @return the childrenAge
     */
    public String getChildrenAge() {
        return childrenAge;
    }

    /**
     * @param childrenAge the childrenAge to set
     */
    public void setChildrenAge(String childrenAge) {
        this.childrenAge = childrenAge;
    }

    /**
     * @return the additionalServices
     */
    public Collection<Service> getAdditionalServices() {
        return additionalServices;
    }

    /**
     * @param additionalServices the additionalServices to set
     */
    public void setAdditionalServices(Collection<Service> additionalServices) {
        this.additionalServices = additionalServices;
    }

    /**
     * @return the reservationStatus
     */
    public Integer getReservationStatus() {
        return reservationStatus;
    }

    /**
     * @param reservationStatus the reservationStatus to set
     */
    public void setReservationStatus(Integer reservationStatus) {
        this.reservationStatus = reservationStatus;
    }

    public Calendar getMadeOn() {
        return madeOn;
    }

    public void setMadeOn(Calendar madeOn) {
        this.madeOn = madeOn;
    }

    public Calendar getConfirmedOn() {
        return confirmedOn;
    }

    public void setConfirmedOn(Calendar confirmedOn) {
        this.confirmedOn = confirmedOn;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

}
