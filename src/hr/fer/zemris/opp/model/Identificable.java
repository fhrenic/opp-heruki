package hr.fer.zemris.opp.model;

public abstract class Identificable {

    public abstract Long getId();

    @Override
    public int hashCode() {
        return getId().intValue();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!obj.getClass().equals(this.getClass())) return false;
        Identificable other = (Identificable) obj;
        return getId().equals(other.getId());
    }
    
    @Override
    public String toString() {
        return getClass().getSimpleName();
    }

}
