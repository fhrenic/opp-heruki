package hr.fer.zemris.opp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author fhrenic
 */
@Entity
@Table(name = "SERVICES")
public class Service extends Identificable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, length = 50)
    private String name;

    @Override
    public String toString() {
        return name;
    }

    // #################################################################################

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the serviceName
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the serviceName to set
     */
    public void setName(String name) {
        this.name = name;
    }

}
