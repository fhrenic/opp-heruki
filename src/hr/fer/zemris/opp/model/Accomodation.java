package hr.fer.zemris.opp.model;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author fhrenic
 */
@Entity
@Table(name = "ACCOMODATIONS")
@Cacheable(true)
public class Accomodation extends Identificable {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private HousingObject housing;

    @Column(nullable = false)
    private Integer floorNumber;

    @Column(nullable = false)
    private Integer roomNumber;

    @ManyToOne
    private Group group;

    @OneToMany(mappedBy = "accomodation", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST,
            orphanRemoval = true)
    private List<Reservation> madeReservations;

    @Override
    public String toString() {
        return group.toString() + ", jedinica " + roomNumber + " na " + floorNumber
                + ". katu u objektu " + housing.toString();
    }

    // #################################################################################

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    /**
     * @return the housing
     */
    public HousingObject getHousing() {
        return housing;
    }

    /**
     * @param housing the housing to set
     */
    public void setHousing(HousingObject housing) {
        this.housing = housing;
    }

    /**
     * @return the floorNumber
     */
    public Integer getFloorNumber() {
        return floorNumber;
    }

    /**
     * @param floorNumber the floorNumber to set
     */
    public void setFloorNumber(Integer floorNumber) {
        this.floorNumber = floorNumber;
    }

    /**
     * @return the roomNumber
     */
    public Integer getRoomNumber() {
        return roomNumber;
    }

    /**
     * @param roomNumber the roomNumber to set
     */
    public void setRoomNumber(Integer roomNumber) {
        this.roomNumber = roomNumber;
    }

    /**
     * @return the madeReservations
     */
    public List<Reservation> getMadeReservations() {
        return madeReservations;
    }

    /**
     * @param madeReservations the madeReservations to set
     */
    public void setMadeReservations(List<Reservation> madeReservations) {
        this.madeReservations = madeReservations;
    }

}
