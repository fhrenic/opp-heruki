package hr.fer.zemris.opp.model;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author fhrenic
 */
@Entity
@Table(name = "HOUSING_OBJECTS")
public class HousingObject extends Identificable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(length = 30, nullable = false)
    private String name;

    @OneToMany(mappedBy = "housing", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST,
            orphanRemoval = true)
    private Collection<Accomodation> accomodations;

    @Override
    public String toString() {
        return name;
    }

    // #################################################################################

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the accomodations
     */
    public Collection<Accomodation> getAccomodations() {
        return accomodations;
    }

    /**
     * @param accomodations the accomodations to set
     */
    public void setAccomodations(Collection<Accomodation> accomodations) {
        this.accomodations = accomodations;
    }

}
