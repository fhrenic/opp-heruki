package hr.fer.zemris.opp.model;

import java.util.Collection;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import hr.fer.zemris.opp.util.Utility;

/**
 * @author fhrenic
 */
@Entity
@Table(name = "USERS")
@Cacheable(true)
public class User extends Identificable {

    // authentication levels
    public static final int GUEST = 0;
    public static final int MEMBER = 1;
    public static final int ADMIN = 2;
    public static final int OWNER = 3;

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, length = 50)
    private String firstname;

    @Column(nullable = false, length = 50)
    private String lastname;

    @Column(nullable = false, length = 50)
    private String nickname;

    @Column(nullable = false, length = 50)
    private String password; // don't store password in database, store hash

    @OneToOne(optional = false, fetch = FetchType.EAGER, cascade = CascadeType.PERSIST,
            orphanRemoval = true)
    private Address address;

    @Column(nullable = false, length = 50)
    private String email;

    @Column(nullable = true, length = 50)
    private String telephone;

    @Column(nullable = false)
    private Integer authLevel; // 0 - normal, 1 - administrator, 2 - owner

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST,
            orphanRemoval = true)
    @OrderBy("fromDate DESC")
    private Collection<Reservation> reservations;

    // #################################################################################

    @Override
    public String toString() {
        return nickname;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the firstName
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstName to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastName
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the nickname
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * @param nickname the nickname to set
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * @return the address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * @param telephone the telephone to set
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * @return the authLevel
     */
    public Integer getAuthLevel() {
        return authLevel;
    }

    /**
     * @param authLevel the authLevel to set
     */
    public void setAuthLevel(Integer authLevel) {
        this.authLevel = authLevel;
    }

    /**
     * @return the reservations
     */
    public Collection<Reservation> getReservations() {
        return reservations;
    }

    /**
     * @param reservations the reservations to set
     */
    public void setReservations(Collection<Reservation> reservations) {
        this.reservations = reservations;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = Utility.getEncoded(password);
    }

}
