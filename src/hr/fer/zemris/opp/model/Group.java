package hr.fer.zemris.opp.model;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "GROUPS")
@Cacheable(true)
public class Group extends Identificable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, length = 100)
    private String name;

    @Column(nullable = false)
    private Integer picture;

    @Column(nullable = false, length = 100)
    private String description;

    @Column(nullable = false, scale = 2) // 2 decimal places
    private Double price;

    @Column(nullable = false, length = 10)
    private String accType;

    @Column(nullable = false)
    private Integer capacityLow;

    @Column(nullable = false)
    private Integer capacityHigh;

    @Column(nullable = false, length = 20)
    private String viewType;

    @OneToMany(mappedBy = "group", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST,
            orphanRemoval = true)
    private List<Accomodation> accomodations;

    @Override
    public String toString() {
        return description;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getAccType() {
        return accType;
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }

    public Integer getCapacityLow() {
        return capacityLow;
    }

    public void setCapacityLow(Integer capacityLow) {
        this.capacityLow = capacityLow;
    }

    public Integer getCapacityHigh() {
        return capacityHigh;
    }

    public void setCapacityHigh(Integer capacityHigh) {
        this.capacityHigh = capacityHigh;
    }

    public String getViewType() {
        return viewType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    public List<Accomodation> getAccomodations() {
        return accomodations;
    }

    public void setAccomodations(List<Accomodation> accomodations) {
        this.accomodations = accomodations;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPicture() {
        return picture;
    }

    public void setPicture(Integer picture) {
        this.picture = picture;
    }

}
