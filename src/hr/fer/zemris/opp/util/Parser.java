package hr.fer.zemris.opp.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import hr.fer.zemris.opp.dao.DAO;
import hr.fer.zemris.opp.dao.DAOProvider;
import hr.fer.zemris.opp.dao.jpa.JPAEMFProvider;
import hr.fer.zemris.opp.model.Accomodation;
import hr.fer.zemris.opp.model.Country;
import hr.fer.zemris.opp.model.Group;
import hr.fer.zemris.opp.model.HousingObject;
import hr.fer.zemris.opp.model.Service;

public class Parser {

    private static final String FOLDER = "DB_FILES";
    private static final String HOUSING_OBJECTS_PATH = "objekti.txt";
    private static final String SERVICE_PATH = "usluge.txt";
    private static final String GROUP_PATH = "grupe.txt";
    private static final String ACCOMODATION_PATH = "accomodations.txt";
    private static final String COUNTRY_PATH = "drzave.txt";

    private static List<HousingObject> housingObjects;
    private static List<Group> groups;
    private static List<Accomodation> accomodations;
    private static List<Service> services;
    private static List<Country> countries;

    public static void main(String[] args) throws IOException {
        String factory = Props.get("factory");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory(factory);
        JPAEMFProvider.setFactory(emf);
        loadAndSave();
        emf.close();
    }

    static {
        parse();
    }

    private static BufferedReader read(String filename) throws IOException {
        InputStream is = Parser.class.getClassLoader().getResourceAsStream(FOLDER + '/' + filename);
        return new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
    }

    public static void loadAndSave() {
        DAO dao = DAOProvider.getDAO();
        housingObjects.forEach(dao::save);
        groups.forEach(dao::save);
        services.forEach(dao::save);
        accomodations.forEach(dao::save);
        countries.forEach(dao::save);
    }

    private static void parse() {
        housingObjects = getList(HOUSING_OBJECTS_PATH, Parser::createHousingObject);
        services = getList(SERVICE_PATH, Parser::createService);
        groups = getList(GROUP_PATH, Parser::createGroup);
        accomodations = getList(ACCOMODATION_PATH, Parser::createAccomodation);
        countries = getList(COUNTRY_PATH, Parser::createCountry);
    }

    private static Service createService(String line) {
        Service service = new Service();
        service.setName(line);
        return service;
    }

    private static HousingObject createHousingObject(String line) {
        HousingObject housingObj = new HousingObject();
        housingObj.setName(line);
        return housingObj;
    }

    private static Group createGroup(String line) {
        String[] split = line.split(",");
        Group group = new Group();

        group.setAccType(Integer.parseInt(split[0]) == 1 ? "apartman" : "soba");
        group.setCapacityLow(Integer.parseInt(split[1]));
        group.setCapacityHigh(Integer.parseInt(split[2]));
        group.setViewType((Integer.parseInt(split[3])) == 1 ? "more" : "park");
        group.setPrice(Double.parseDouble(split[4]));
        group.setName(split[5]);
        group.setPicture(Integer.parseInt(split[6]));

        String description = "5 zvjezdica, pogled na " + group.getViewType();
        group.setDescription(description);

        return group;
    }

    private static Accomodation createAccomodation(String line) {
        String[] split = line.split(",");
        Accomodation acc = new Accomodation();
        acc.setHousing(housingObjects.get(Integer.parseInt(split[0])));
        acc.setFloorNumber(Integer.parseInt(split[1]));
        acc.setRoomNumber(Integer.parseInt(split[2]));
        acc.setGroup(groups.get(Integer.parseInt(split[3])));
        return acc;
    }

    private static Country createCountry(String line) {
        Country country = new Country();
        country.setName(line);
        return country;
    }

    private static <R> List<R> getList(String path, Function<String, R> creator) {
        List<R> list = new ArrayList<>();

        try {
            list = read(path).lines().map(creator::apply).collect(Collectors.toList());
        } catch (IOException ignorable) {
        }
        return list;
    }

}
