package hr.fer.zemris.opp.util;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.opp.model.Group;

/**
 * A helper class that provides some useful utility methods and constants for
 * this project.
 * 
 * @author fhrenic
 */
public class Utility {

    private static final Pattern datePattern = Pattern.compile("(\\d{4})-(\\d{2})-(\\d{2})");

    public static final String ENCODING = "UTF-8";
    public static final Charset CHARSET = StandardCharsets.UTF_8;

    private static final SimpleDateFormat HTML_SDF;
    private static final MessageDigest SHA_DIGEST;
    private static final long DAY_MILLIS = 24 * 60 * 60 * 1000;

    public static final Calendar SEASON_START = Calendar.getInstance();
    public static final Calendar SEASON_END = Calendar.getInstance();

    static {
        try {
            SHA_DIGEST = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException nsae) {
            throw new RuntimeException(nsae);
            // ignoring it, because there is a SHA-1 algorithm
        }
        HTML_SDF = new SimpleDateFormat("YYYY-MM-DD");

        SEASON_START.set(0, Calendar.MAY, 1, 0, 0, 0);
        SEASON_START.set(Calendar.MILLISECOND, 0);
        SEASON_END.set(0, Calendar.SEPTEMBER, 30, 23, 59, 59);
        SEASON_END.set(Calendar.MILLISECOND, 0);
    }

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map,
            boolean asc) {
        int x = asc ? 1 : -1;
        List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(map.entrySet());
        Collections.sort(list, (e1, e2) -> x * e1.getValue().compareTo(e2.getValue()));
        Map<K, V> result = new LinkedHashMap<K, V>();
        list.forEach(e -> result.put(e.getKey(), e.getValue()));
        return result;
    }

    /**
     * Returns the encoded text.
     * 
     * @return hex encoded text
     */
    public static String getEncoded(String text) {
        byte[] buffer = text.getBytes(CHARSET);
        return byteToHex(SHA_DIGEST.digest(buffer));
    }

    /**
     * Checks if the encoded text is equal to the given encoded parameter.
     * 
     * @param encoded some hex coding
     * @param text text to encode
     * @return if the given encoded text matches encoded parameter
     */
    public static boolean checkEquals(String encoded, String text) {
        String encodedText = getEncoded(text);
        return encoded.equals(encodedText);
    }

    /**
     * Returns string representation of a byte array.
     * 
     * @param bytes bytes to represent
     * @return string representation
     */
    private static String byteToHex(byte[] bytes) {
        StringBuffer buff = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            buff.append(Integer.toHexString(bytes[i] & 0xFF));
        }
        return buff.toString();
    }

    public static double price(Group g, Calendar from, Calendar to) {
        return g.getPrice() * (daysBetween(to, from) + 1);
    }

    //dates

    public static boolean inSeason(Calendar from, Calendar to) {
        int year = from.get(Calendar.YEAR);
        SEASON_START.set(Calendar.YEAR, year);
        SEASON_END.set(Calendar.YEAR, year);
        return !SEASON_START.after(from) && !SEASON_END.before(to);
    }

    public static void setDate(String param, HttpServletRequest req, Calendar date) {
        String dateStr = req.getParameter(param);
        Matcher mat = datePattern.matcher(dateStr);
        mat.find();
        int y = Integer.parseInt(mat.group(1));
        int m = Integer.parseInt(mat.group(2));
        int d = Integer.parseInt(mat.group(3));
        date.set(y, m - 1, d);
    }

    public static Calendar today() {
        return Calendar.getInstance();
    }

    public static String todaysDate() {
        return HTML_SDF.format(today().getTime());
    }

    public static long daysBetween(Calendar c1, Calendar c2) {
        long m1 = removeHMSM(c1).getTimeInMillis();
        long m2 = removeHMSM(c2).getTimeInMillis();
        long m = (m1 - m2) / DAY_MILLIS;
        return Math.abs(m);
    }

    private static Calendar removeHMSM(Calendar c) {
        Calendar c2 = (Calendar) c.clone();
        c2.set(Calendar.HOUR, 0);
        c2.set(Calendar.MINUTE, 0);
        c2.set(Calendar.SECOND, 0);
        c2.set(Calendar.MILLISECOND, 0);
        return c2;
    }

    public static String dateToString(Calendar dateFrom) {
        int y = dateFrom.get(Calendar.YEAR);
        int m = dateFrom.get(Calendar.MONTH) + 1; // jer su idioti
        int d = dateFrom.get(Calendar.DAY_OF_MONTH);
        return String.format("%02d/%02d/%4d", d, m, y);
    }

    // mail

    public static void sendMailToUser(String to, String subject, String body) {
        Thread sender = new Thread(() -> sendMailToUserWrapped(to, subject, body));
        sender.setDaemon(true);
        sender.run();
    }

    private static void sendMailToUserWrapped(String to, String subject, String body) {

        Properties properties = new Properties();
        
        String[] props = new String[] { "mail.smtp.host", "mail.smtp.socketFactory.port",
                "mail.smtp.socketFactory.class", "mail.smtp.auth", "mail.smtp.port" };
        for (String prop : props) {
            properties.put(prop, Props.get(prop));
        }

        Session session = Session.getDefaultInstance(properties, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                String username = Props.get("mail.username");
                String password = Props.get("mail.password");
                return new PasswordAuthentication(username, password);
            }
        });

        try {

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress("support@heruki.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.setSubject(subject);
            message.setText(body, "UTF-8", "html");

            Transport.send(message);

        } catch (MessagingException e) {
        }
    }

    /**
     * Singleton, can't be instantiated
     */
    private Utility() {
    }

}