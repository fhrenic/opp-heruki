package hr.fer.zemris.opp.util;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.Rotation;

public class Reports {

    public static final int CHART = 1;
    public static final int XLS = 2;

    private Reports() {
    }

    /**
     * Y dimenzija slike.
     */
    private static final int Y_DIM = Integer.parseInt(Props.get("report.image.y"));

    /**
     * X dimenzija slike.
     */
    private static final int X_DIM = Integer.parseInt(Props.get("report.image.x"));

    private static JFreeChart createChart(final PieDataset dataset, final String title) {

        JFreeChart chart = ChartFactory.createPieChart3D(title, // chart title
                dataset, // data
                true, // include legend
                true, false);

        PiePlot3D plot = (PiePlot3D) chart.getPlot();
        plot.setStartAngle(120);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(1.0f);
        plot.setIgnoreZeroValues(true);
        plot.setNoDataMessage("Nema podataka");
        return chart;

    }

    public static <T extends Object> void generate(int type, String name, Map<T, Long> data,
            OutputStream stream) throws IOException {
        if (type == CHART) stream.write(getPng(name, data));
        if (type == XLS) getXls(name, data).write(stream);
    }

    private static <T extends Object> byte[] getPng(String title, Map<T, Long> data) {
        // This will create the dataset

        Map<T, Long> sortedData = Utility.sortByValue(data, false);
        int count = 0;
        int numberOfOthers = 0;

        DefaultPieDataset dataset = new DefaultPieDataset();
        for (T key : sortedData.keySet()) {
            Long value = sortedData.get(key);
            if (value == 0) break;
            if (count++ >= 9) numberOfOthers += value;
            else dataset.setValue(key.toString(), value);
        }

        if (numberOfOthers > 0) {
            dataset.setValue("Ostali", numberOfOthers);
        }

        // based on the dataset we create the chart
        JFreeChart chart = createChart(dataset, title);

        BufferedImage bim = chart.createBufferedImage(X_DIM, Y_DIM);

        try {
            return ChartUtilities.encodeAsPNG(bim);
        } catch (IOException e) {
            return null;
        }
    }

    private static <T extends Object> HSSFWorkbook getXls(String name, Map<T, Long> data) {

        HSSFWorkbook workbook = new HSSFWorkbook();

        HSSFSheet sheet = workbook.createSheet(name);
        HSSFRow row = sheet.createRow(0);
        row.createCell(0).setCellValue("Naziv");
        row.createCell(1).setCellValue("Broj rezervacija");

        int rowCnt = 1;

        TreeMap<T, Long> sorted = new TreeMap<>((a, b) -> data.get(a).compareTo(data.get(b)));
        sorted.putAll(data);

        for (Entry<T, Long> entry : data.entrySet()) {
            row = sheet.createRow(rowCnt++);
            row.createCell(0).setCellValue(entry.getKey().toString());
            row.createCell(1).setCellValue(entry.getValue());
        }

        return workbook;
    }

}
