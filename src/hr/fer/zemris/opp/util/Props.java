package hr.fer.zemris.opp.util;

import java.util.ResourceBundle;

/**
 * Properties class that reads the external file in which are strings. That way
 * nothing is hardcoded.
 * 
 * @author fhrenic
 */
public class Props {

    private static final String PROPS_NAME = "project";
    private static final ResourceBundle BUNDLE;

    static {
        BUNDLE = ResourceBundle.getBundle(PROPS_NAME);
    }

    /**
     * Returns the value that is binded to the given key.
     * 
     * @param key some key
     * @return value
     */
    public static String get(String key) {
        return BUNDLE.getString(key);
    }

    /**
     * Singleton, static methods only
     */
    private Props() {
    }

}
