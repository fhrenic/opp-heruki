#! /usr/bin/python
# -*- coding: utf-8 -*-
def writeFile(file, content):
    # content should be list(rows), row = list(elements)
    sep = ','
    s = '\n'.join( sep.join(str(element) for element in row) for row in content )
    open(file,"w").write(s)

def objects():
    # ime
    return [["A"], ["B"], ["C"], ["D"]]

def services():
    # ime
    return [["Rezervirano parkirno mjesto"], ["Bezicni pristup internetu"], ["Satelitski TV prijamnik"]]

def groups():
    # vrsta[ 1=apartman, 2=soba ], kapacitetLow, kapacitetHigh, pogledNa[ 1=more, 2=park ], cijena, ime, picture
    pA = 500 # price apartman
    pS = 200 # price soba

    return [ (1, 2, 4, 1, pA, "Apartman King", 1),
             (1, 2, 4, 2, pA, "Apartman Royal", 2),
             (1, 6, 8, 1, pA, "Apartman Turbo", 3),
             (1, 6, 8, 2, pA, "Apartman Show", 4),
             (2, 2, 3, 1, pS, "Soba Queen", 5),
             (2, 2, 3, 2, pS, "Soba Westside", 6) ]

def accomodations():
    # objekt, kat, broj sobe, grupa

    def genAcc(obId, floorFunc, roomFunc, groupFunc, n):
        l = list()
        for i in range(n):
            l.append( (obId, floorFunc(i), roomFunc(i), groupFunc(i)) )
        return l


    floorFunc = lambda x: x/4 + 1
    roomFunc  = lambda x: x%4 + 1
    groupFunc = lambda x: (x%4)/2

    A = genAcc(0, floorFunc, roomFunc, groupFunc, 8)
    B = genAcc(1, floorFunc, roomFunc, groupFunc, 8)

    floorFunc = lambda x: x/2 + 1
    roomFunc  = lambda x: x%2 + 1
    groupFunc = lambda x: x%2 + 2

    C = genAcc(2, floorFunc, roomFunc, groupFunc, 4)

    floorFunc = lambda x: 1 if x<2 else 2
    roomFunc  = lambda x: x+1 if x<2 else x-1
    groupFunc = lambda x: (x if x<2 else (x-2)/4 + 2)+2

    D = genAcc(3, floorFunc, roomFunc, groupFunc, 10)

    return A+B+C+D

def run():
    writeFile("objekti.txt", objects())
    writeFile("usluge.txt", services())
    writeFile("grupe.txt", groups())
    writeFile("accomodations.txt", accomodations())

run()

